# Popularity assessment of top websites across CERN Web Frameworks

These datasets are created by Elasticsearch log analysis.
They were used in our paper [Building a Kubernetes infrastructure for CERN's Content Management Systems](https://zenodo.org/record/4923215).

### [WebEOS](webeos.csv)

info | value
--- | ---
elasticsearch endpoint | https://monit-timber-webinfra.cern.ch/
access date | 2021-06-10
timespan | 1 month
index | `monit_private_webinfra_logs_webeos-apache*`
query |
metric | Count
buckets | `terms(data.servername, order_by=count, size=100)`
file | [webeos.csv](webeos.csv)

This dataset is built on the number of raw HTTP requests to each website,
not unique client IPs. It's less indicative of a site's "publicity".

![top sites pie](top_sites_pie_webeos.png)

### [PaaS](paas.csv)

info | value
--- | ---
elasticsearch endpoint | https://monit-timber-openshift.cern.ch/
access date | 2021-06-10
timespan | 1 month
index | `monit_private_openshift_*`
query | `data.program:"haproxy"`
metric | Unique count of `data.client_ip`
buckets | `terms(data.namespace_name, order_by=count, size=100)`
file | [paas.csv](paas.csv)

This dataset is build on the number of unique client IPs visiting each website,
indicating how widespread access is, therefore interpreted as a measure of "publicity".

![top sites pie](top_sites_pie_paas.png)

### [Drupal](drupal.csv)

info | value
--- | ---
elasticsearch endpoint | https://monit-timber-drupal-prod8.cern.ch/
access date | 2021-06-10
timespan | 1 month
index | `monit_private_drupal_logs_prod8-*`
query | `data.program:"httpd"`
metric | Unique count of `data.clientip`
buckets | `terms(data.sitename, order_by=count, size=100)`
file | [drupal.csv](drupal.csv)

This dataset is build on the number of unique client IPs visiting each website,
indicating how widespread access is, therefore interpreted as a measure of "publicity".

![top sites pie](top_sites_pie_drupal.png)


