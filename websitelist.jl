### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ bc3fc7ce-d429-11eb-2848-83c860939e87
begin
	import Pkg
	Pkg.activate(".")
	using FileIO
	using JLD2
	#using JLD
	using HDF5
	using DataFrames
	using StatsBase
	using Chain
	using Printf
	using DSP
	using Plots
	using PlutoUI
	using TimeSeries
end

# ╔═╡ ce0695b8-a44f-40ce-a391-ed0c8e3ad294
md"# A notebook for plotting websites data
This notebook is run by modifying the last two cells with the websites that should be plotted. First we import all relevant packages using the git repo environment."

# ╔═╡ c09ff6b3-fcef-4ce1-889a-53e6f84ceae8
include("/src/loadfile.jl")

# ╔═╡ 944fcac6-197e-4b5a-9e45-740742200428
function time_series(dict)
	df = dict["ts"]
	df[isnan.(df."error rate (15m)"), :"error rate (15m)"] .= 0 #set NaN error rates to zero
    err = df[!,"error rate (15m)"]
    m = maximum(err[err .!= Inf])
    df[isinf.(df."error rate (15m)"), :"error rate (15m)"] .= 10*m #set Inf error rates to 10max
    df
end

# ╔═╡ 2be41676-a74a-46e9-87ec-d3b618ce4e18
md"The result from the metric filtering from the `src/loadmetrics.jl` script is loaded."

# ╔═╡ 35a55ab9-03c0-40d8-94b5-8e517f8c5aa6
d = load("data.jld2")["data"]

# ╔═╡ 42a2847c-0257-4c4e-8dca-d3af8cb9deae
tol = 0.2

# ╔═╡ 70f49b72-9568-4b0e-b386-2bb2092711fe
md"The relevant webpages are:"

# ╔═╡ 734160cf-3f29-40d7-9d28-d1a53b58a5df
significant = filter(x -> last(x)[2] > tol, d)

# ╔═╡ 9438c025-9247-4ced-833a-f698aac70191
weblist2 = [(k,v[2]) for (k,v) in significant]

# ╔═╡ 9da2990b-8465-4062-a7ec-a6620ef6e51d
md"We sort the list after how interesting they are according to the (very crude) metric."

# ╔═╡ dfed7dd3-5e28-4331-b3df-9d11c4e926b8
sort!(weblist2, by = x -> -last(x))

# ╔═╡ 9f9a6052-b4a6-4e59-bd67-02a2ab337a5c
weblist = [k for (k,v) in weblist2]

# ╔═╡ 3aa31a50-6fb0-4560-a174-fb87dcd1306f
length(significant)

# ╔═╡ 3dc16d39-9f1c-42af-9a63-c9095cea54d1
md"We have 128 significant websides according to this tolerance level."

# ╔═╡ bb9bdc4e-baa1-4219-b52e-7485e4363ec4
md"The missing webpages are:"

# ╔═╡ 86f163df-6392-4d28-826d-7461a8a5271d
missing = filter(x -> last(x)[2] == -1.0, d)

# ╔═╡ a44749a8-8a48-4061-be4b-3180ace80e6f
keys(missing)

# ╔═╡ 7a7f14df-8c75-4e0e-a345-07d4a5175ddd
length(missing)

# ╔═╡ ff69ec2d-8eba-4996-a713-f075ebc80d5d
md"The function for plotting a heatmap of the spectrogram of the connection rate for a website, called with the website data name."

# ╔═╡ 13947e51-edf9-4e7d-8fad-fe38e5f93638
function spect(filename)
	file = load_data(filename)
	df = time_series(file)
    connect = df[!,"connection irate"]
    mean_centered = connect .- mean(connect)
    p = spectrogram(mean_centered, fs = 1/60, window = hamming)
    heatmap(power(p),ylabel = "Frequency", xlabel = "Time bin")
    #savefig(@sprintf "tmp-plots/%s:spectrogram" replace(filename, ".jld2" => ""))
end

# ╔═╡ baceba88-1de3-437a-9aae-d02e48bff0c0
md"Function that plots all the metrics for one website along with autocorrelation and partial autocorrelation functions if applicable. The plots are not displayed in the notebook but saved in the `tmp-plots` folder for watching later. This folder is ignored by the repo so the plots are only locally stored."

# ╔═╡ 7b9ae360-7302-4886-a9b2-7430cd0cc324
function plotting(filename)
    file = load_data(filename)
	df = time_series(file)
    columns = (names(df))[(begin+1):end]
    for column in columns
        p = plot(df[!,"time"], df[!,column], xlabel = "Time", title = "Time Series", ylabel = @sprintf "%s" column)
        try 
            acf = plot(autocor(df[!,column]), xlabel = "Lag", ylabel = "Correlation", title = "ACF", ylims = (0,1))
            pac = plot(pacf(df[!,column],collect(1:37)), xlabel = "Lag", ylabel = "Correlation", title = "PACF", ylims = (0,1))
            plot(p, acf, pac, layout = 3)
        catch e
            plot(p)
        end
        savefig(@sprintf "tmp-plots/%s:%s" replace(filename, ".jld2" => "") replace(column, " " => "-"))
    end
end

# ╔═╡ dcd4cb2e-8532-42c9-8ca7-762213437cd4
md"Function that plots all the interesting cross covariance plots (the ones that have more correlation than 15% at some lag). The function is called with the name of the website data. These plots are also saved in `tmp-plots`."

# ╔═╡ c46fdd3c-6f95-430e-91ab-32219cacdcc0
function crosscovariance(filename)
    file = load_data(filename)
	df = time_series(file)
    ts = TimeArray(df, timestamp = :time)
    mat = values(ts)
    c = crosscor(mat,mat, 0:40)
    columns = (names(df))[(begin+1):end]
    for i in eachindex(columns)
        for j in 1:(i-1) # We go through the indices less than current column, so we dont go through it twice
            if any(c[:,i,j] .> 0.15) #If correlation exceeds threshold then plot it
                plot(c[:,i,j],ylims = (0,1),xlab = "Lags", ylab = "Correlation",title = @sprintf "Cross corr.: %s and %s" columns[i] columns[j])|>display
                savefig(@sprintf "tmp-plots/%s:correlation:%s:%s" replace(filename, ".jld2" => "") replace(columns[i], " " => "-") replace(columns[j], " " => "-"))
            end
        end
    end
end

# ╔═╡ 8c99fe94-75e5-4742-b0f8-c49cce83efa6
md"The two cells that should be run for each interesting website. We chose the top 4 ones in weblist, and from before the lhcbproject."

# ╔═╡ 3e0a3add-c103-455b-8ae9-bc41eb43acb6
plotting(weblist[4])

# ╔═╡ 3671a16d-5124-46ce-ad47-9ccf96423ffd
crosscovariance(weblist[4])

# ╔═╡ ab6a4bde-b844-48f1-827f-b76919673cb2
md"The sites we will consider closer are cvmrepo, xrootd, dss-ci-repo, lcgpackages and lhcbproject."

# ╔═╡ Cell order:
# ╟─ce0695b8-a44f-40ce-a391-ed0c8e3ad294
# ╠═bc3fc7ce-d429-11eb-2848-83c860939e87
# ╠═c09ff6b3-fcef-4ce1-889a-53e6f84ceae8
# ╠═fbd8cd40-8d36-4f14-b2f6-f1cba3d0e540
# ╠═ae9e6f68-28d3-478c-a91b-231cfe60af5b
# ╠═944fcac6-197e-4b5a-9e45-740742200428
# ╟─2be41676-a74a-46e9-87ec-d3b618ce4e18
# ╠═35a55ab9-03c0-40d8-94b5-8e517f8c5aa6
# ╠═42a2847c-0257-4c4e-8dca-d3af8cb9deae
# ╟─70f49b72-9568-4b0e-b386-2bb2092711fe
# ╠═734160cf-3f29-40d7-9d28-d1a53b58a5df
# ╠═9438c025-9247-4ced-833a-f698aac70191
# ╟─9da2990b-8465-4062-a7ec-a6620ef6e51d
# ╠═dfed7dd3-5e28-4331-b3df-9d11c4e926b8
# ╠═9f9a6052-b4a6-4e59-bd67-02a2ab337a5c
# ╠═3aa31a50-6fb0-4560-a174-fb87dcd1306f
# ╟─3dc16d39-9f1c-42af-9a63-c9095cea54d1
# ╠═bb9bdc4e-baa1-4219-b52e-7485e4363ec4
# ╠═86f163df-6392-4d28-826d-7461a8a5271d
# ╠═a44749a8-8a48-4061-be4b-3180ace80e6f
# ╠═7a7f14df-8c75-4e0e-a345-07d4a5175ddd
# ╟─ff69ec2d-8eba-4996-a713-f075ebc80d5d
# ╠═13947e51-edf9-4e7d-8fad-fe38e5f93638
# ╟─baceba88-1de3-437a-9aae-d02e48bff0c0
# ╠═7b9ae360-7302-4886-a9b2-7430cd0cc324
# ╟─dcd4cb2e-8532-42c9-8ca7-762213437cd4
# ╠═c46fdd3c-6f95-430e-91ab-32219cacdcc0
# ╟─8c99fe94-75e5-4742-b0f8-c49cce83efa6
# ╠═3e0a3add-c103-455b-8ae9-bc41eb43acb6
# ╠═3671a16d-5124-46ce-ad47-9ccf96423ffd
# ╟─ab6a4bde-b844-48f1-827f-b76919673cb2
