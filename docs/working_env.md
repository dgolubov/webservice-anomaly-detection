# Working environment

Goal: a remote working environment accessible through VSCode with Julia preconfigured

Requirements:
- Accessible through VSCode
- Persistence of VSCode settings and home directory `/home/jovyan`

### Persistence

The running container has a Persistent Volume mounted at `/home/jovyan`, therefore any filesystem changes in this directory are persisted.
The container image already has some files there, so an init container with the same image copies them to the PVC.

## Setup first connection

[These docs](https://code.visualstudio.com/docs/remote/attach-container#_attach-to-a-container-in-a-kubernetes-cluster) show how to connect to this working environment.
Steps:
1. Install extensions "Kubernetes" and "Remote - Containers"
1. Point the extension to the cluster with the given `kubeconfig`
    - Ctrl+Shift+P -> "set kubeconfig"
1. From the Kubernetes extension tab: default -> workloads -> pods -> vscodeserver... right click "Attach Visual Studio Code"

