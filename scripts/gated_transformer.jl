cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();
import Chain: @chain
using Flux, CUDA, Transformers, Statistics, CategoricalArrays
using YAML, Dates, Transformers.Basic, Transformers.Stacks, Printf, Plots
##
include("../src/dataloader.jl")
include("../src/transformer_model.jl")
include("../src/trainwithprogress.jl")

# Parameters
parameters= YAML.load_file("parameters.yaml")
sitename= "lhcbproject"
segmentLength= 15

# Hyperparams
parameters["transformer"]= Dict(
    "width"=> 256,
)

# 1. get data

preprocessedData, vocab, data, target= dataloaderTransformerForecasting("webeos/$sitename.jld2",
    segmentLength=segmentLength, batchSize=100, quantizationLevelsPerMetric=[5,6,5,10,6,25,6])
d_metric= size(data|>first,2)
vocabularyLength= (vocab.|>length|>maximum) + 1
targetvocab = 0:length(vocab[6])
unknownLabel= 0
hyperP= parameters["transformer"]
#julia> Matrix(data[:,2:end])|> sparsity
#7-element Vector{Float64}:
#0.002890749601275888
#0.13412081339712922
#0.013905502392344449
#0.5297547846889952
#0.13417065390749605
#0.9474182615629984
#0.1335227272727273

# 2. define model

# Attempted to use Float16, but apparently GEMM routines are missing
# Support might be on the way https://github.com/JuliaGPU/CUDA.jl/issues/391
f16(m)= Flux.paramtype(Float16, m)

N = 2
encoder = Stack(
    @nntopo(x → t → t → $N),
    LinearPositionEmbedding(d_metric,hyperP["width"]),
    Dropout(0.1),
    [Transformer(hyperP["width"], 8, 32, 1024) for i = 1:N]...
) |> f32 |> gpu
# Classic decoder receiving hidden state
decoder = Stack(
    @nntopo((x,m):x → t → (t:(t, m) → t:(t, m)) → $N:t → c),
    LinearPositionEmbedding(d_metric,hyperP["width"]),
    Dropout(0.1),
    [TransformerDecoder(hyperP["width"], 8, 32, 1024) for i = 1:N]...,
    Positionwise(Dense(hyperP["width"], vocabularyLength), logsoftmax)
) |> f32 |> gpu
# Decoder without hidden state
decoderSingle = Stack(
    @nntopo(x → t → (t → t) → $N:t → c),
    LinearPositionEmbedding(d_metric,hyperP["width"]),
    Dropout(0.1),
    [Transformer(hyperP["width"], 8, 32, 1024, future=false) for i = 1:N]...,
    Positionwise(Dense(hyperP["width"], vocabularyLength), logsoftmax)
) |> f32 |> gpu

## 3. Train

trainable_params = Flux.params(decoderSingle)
optimizer = ADAM(1e-6)

data= data|>gpu
target= target|>gpu
train_data = zip(data, target)
nbatch = length(train_data)

function train!(epochs)
    @info "Starting training" start_loss = mean([decodeOnlyLoss(input,target) for (input,target) in train_data])/nbatch
    for i in collect(1:epochs)
        @info "Epoch $i"
        trainwithprogress!(decodeOnlyLoss, trainable_params, train_data, optimizer)
        epoch_loss = mean([decodeOnlyLoss(input,target) for (input,target) in train_data])/nbatch
        @info "Finished epoch" epoch_loss
        #total_plot(preprocessedData,train_data,i,epoch_loss)
    end
    @info "Finished training" stop_loss = mean([decodeOnlyLoss(input,target) for (input,target) in train_data])/nbatch
end
