### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ efddacd2-e498-11eb-0505-7b22750899b6
begin
	## Example for using TPA-LSTM
	# Use the pkg environment of the repo
	# NOTE: Pkg.activate inside the script looks for the filepath relative to the present working directory of the Julia session.
	cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();
	@info "Loading packages"
	using Flux, BSON, Plots, TimeSeries, SliceMap, JuliennedArrays
	using YAML
	import Chain as ChainPipes
end

# ╔═╡ c2d29619-6265-4a29-aa8c-7df82feb8d81
begin
	## Load libs
	#include("src/flux/FluxArchitectures/shared/Sequentialize.jl")
	#include("src/flux/FluxArchitectures/shared/StackedLSTM.jl")
	#include("src/flux/FluxArchitectures/data/dataloader.jl")
	#include("src/flux/FluxArchitectures/TPA-LSTM/TPALSTM.jl")
	include("src/dataloader.jl")
end

# ╔═╡ 2d76f44b-e7c7-4aa6-bf38-b5412a5399f8
# Global dict with project parameters, such as the data path
parameters= YAML.load_file("parameters.yaml")

# ╔═╡ c203234d-da8b-45e0-8045-6496bafaea19
begin
	# Load some sample data
	@info "Loading data"
	poollength = 15  #10
	horizon = 5  #6
	datalength = 1000  #2000
	file = "webeos/cvmrepo.jld2"
	input, target = get_website_data(file, poollength, datalength, horizon) |> gpu
end

# ╔═╡ 7434711c-b906-40dc-a5e4-f40eff13d8fd
begin
	# size(input) =(7, 15, 1, 1000)
	# size(target) = (1000,)
	cd("src/flux/FluxArchitectures/data/")
	_input, _target = get_data(:solar, poollength, datalength, horizon) |> gpu
	cd("../../../../")
	# size(_input) = (137, 15, 1, 1000)
	# size(_target) = (1000,)
end

# ╔═╡ c3c474ef-98c5-4ffa-ba22-6de8cf332bb9
size(input)

# ╔═╡ de0c85d5-4447-48ae-9427-95d0d1f14c48
input

# ╔═╡ e45806a8-1a12-4785-bb60-ce3a2fa65b03
size(_input)

# ╔═╡ 8d48a8a3-e6dd-4134-adb1-db9ef9b8c5b7
plot(_input[1,1,1,:])

# ╔═╡ 1eb8a44c-a9d3-46b7-9ac7-4e7e20ccc2ea
plot(_input[1,:,1,20])

# ╔═╡ 382a3d27-2e16-4fe4-bacc-7a85362844b0
plot(_target)

# ╔═╡ bf3817fc-4b8f-48ac-b652-adbf233aa96c
plot(input[:,1,1,:]')

# ╔═╡ 03c19717-af16-4d5c-afd7-5a965a391933
plot(target)

# ╔═╡ ff6ae9b1-7160-4252-a27f-5bb93a63b6cd
plot(_input[:,1,1,200])

# ╔═╡ 2bb7f1b1-1822-4a1c-ba0e-f725d8fcd786
plot(input[:,1,1,200])

# ╔═╡ Cell order:
# ╠═efddacd2-e498-11eb-0505-7b22750899b6
# ╠═c2d29619-6265-4a29-aa8c-7df82feb8d81
# ╠═2d76f44b-e7c7-4aa6-bf38-b5412a5399f8
# ╠═c203234d-da8b-45e0-8045-6496bafaea19
# ╠═7434711c-b906-40dc-a5e4-f40eff13d8fd
# ╠═c3c474ef-98c5-4ffa-ba22-6de8cf332bb9
# ╠═de0c85d5-4447-48ae-9427-95d0d1f14c48
# ╠═e45806a8-1a12-4785-bb60-ce3a2fa65b03
# ╠═8d48a8a3-e6dd-4134-adb1-db9ef9b8c5b7
# ╠═1eb8a44c-a9d3-46b7-9ac7-4e7e20ccc2ea
# ╠═382a3d27-2e16-4fe4-bacc-7a85362844b0
# ╠═bf3817fc-4b8f-48ac-b652-adbf233aa96c
# ╠═03c19717-af16-4d5c-afd7-5a965a391933
# ╠═ff6ae9b1-7160-4252-a27f-5bb93a63b6cd
# ╠═2bb7f1b1-1822-4a1c-ba0e-f725d8fcd786
