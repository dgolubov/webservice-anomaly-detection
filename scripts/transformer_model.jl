cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();

@info "Loading packages"
import Chain: @chain
using Flux, CUDA, Transformers, Statistics
using Flux: onehot
using Transformers.Basic #for loading the positional embedding
using Random
Random.seed!(0)

# Copy example

labels = collect(1:10)
startsym = 11
endsym = 12
unksym = 0
labels = [unksym, startsym, endsym, labels...]
vocab = Vocabulary(labels, unksym)
#function for generate training datas
sample_data() = (d = rand(1:10, 10); (d,d))
#function for adding start & end symbol
preprocess(x) = [startsym, x..., endsym]

@show sample = preprocess.(sample_data())
@show encoded_sample = vocab(sample[1]) # use Vocabulary to encode the training data

# NOTE: vocabulary is essentially quantization. If `sample` is a real valued time series,
# `encoded_sample` should be its quantization.
#
# - `size(encoded_sample)`: 12×1, the length of the sequence/segment/window

# model

#define a Word embedding layer which turn word index to word vector
embed = Embed(512, length(vocab)) #|> gpu
#define a position embedding layer metioned above
pe = PositionEmbedding(512) #|> gpu

#wrapper for get embedding (includes positional embedding)
function embedding(x)
    wordEmbedding = embed(x, inv(sqrt(512)))
    wordEmbedding .+ pe(wordEmbedding)
end

#define 2 layer of transformer
encode_t1 = Transformer(512, 8, 64, 2048) #|> gpu
encode_t2 = Transformer(512, 8, 64, 2048) #|> gpu

#define 2 layer of transformer decoder
decode_t1 = TransformerDecoder(512, 8, 64, 2048) #|> gpu
decode_t2 = TransformerDecoder(512, 8, 64, 2048) #|> gpu

#define the layer to get the final output probabilities
linear = Positionwise(Dense(512, length(vocab)), logsoftmax) #|> gpu

"""
    encoder_forward(x)

This is the entire "encoder" part of the Transformer. It takes a raw data sample, embeds it, adds positional encoding,
and passes it through the defined number of `Transformer` layers.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).
"""
encoder_forward(x) =  @chain x begin
    embedding
    encode_t1
    encode_t2
end

"""
    decoder_forward(x, context)

This is the entire decoder stack. It takes a sequence `x` (or the start of a sequence + UNK symbols if doing iterative prediction)
and a `context` vector, which is normally the output of an encoder stack.
It produces a sequence as output.

Could be implemented with a [`Stack`](https://chengchingwen.github.io/Transformers.jl/dev/stacks/#The-Stack-NNTopo-DSL).
"""
decoder_forward(x, context)= @chain x begin
    embedding
    decode_t1(_, context)
    decode_t2(_, context)
    linear
end

# Produce an encoding and a decoding

# The encoding is a large matrix: 512×12, or {transformer size}×{sequence length}
enc = encoder_forward(encoded_sample)
# Given a sequence, we get the probability distribution over the entire vocabulary
probs = decoder_forward(encoded_sample, enc)

## Loss function
function smooth(et)
    sm = fill!(similar(et, Float32), 1e-6/size(embed, 2))
    p = sm .* (1 .+ -et)
    label = p .+ et .* (1 - convert(Float32, 1e-6))
end
Flux.@nograd smooth

# Fix the onehot embedding
targetEmbeddingInVocab(target, vocab)= @chain target begin
    Flux.onehotbatch(_, vocab.list, vocab.unki)
    # Would be using this one, but the author is enforcing Float32 types and breaks autodiff
    #Flux.Losses.label_smoothing(_, 1e-6)
    smooth
end

"""
    copy_objective(x, target)

The objective (or "loss") function defines what the model should learn.
`copy_objective` teaches the transformer to copy `target` to its output.

It passes `x` to the encoder part of the transformer and `target` to the decoder part.
The decoder computes the probability of each output given `target` over the entire vocabulary.
Then it computes the divergence of the decoder's probability distribution from the "true" distribution.
"""
function copy_objective(x, target)
    enc = encoder_forward(x)
    predictionDistribution = decoder_forward(target, enc)
    logkldivergence(targetEmbeddingInVocab(target,vocab)[:, 2:end, :], predictionDistribution[:, 1:end-1, :])
    #Flux.Losses.kldivergence(label[:, 2:end, :].|> exp, probs[:, 1:end-1, :].|> exp)
end
copy_objective_batch(x, target; agg=Statistics.mean)= copy_objective.(x,target)|> agg

"""
    forecast1_decoderOnly_objective(x, target)

Loss function for predicting with a probabilistic Decoder, that doesn't have a paired encoder.
The decoder's output is compared with the distribution of the target across the vocabulary.
The target must be shifted accordingly a priori (the data loader should prepare `train_y` that are shifted as necessary).
"""
function forecast1_decoderOnly_objective(x, target)
    # this is a decoder that isn't matched with an encoder and takes only 1 input
    predictionDistribution= decoderOnly_forward(x)
    logkldivergence(targetEmbeddingInVocab(target,vocab), predictionDistribution)
end

# Train

# collect all the parameters
trainable_params = Flux.params(embed, pe, encode_t1, encode_t2, decode_t1, decode_t2, linear)
optimizer = ADAM(1e-4)

function get_dataloaders(batch_size; shuffle=false)
    data= [sample_data() for i = 1:128]  # random data points
    x = map(x-> preprocess(x[1]), data)
    y = map(x-> preprocess(x[2]), data)
    train_x, train_y = vocab.(x), vocab.(y) #encode the data
    #train_x, train_y = todevice(train_x, train_y) # move to gpu
    train_loader= Flux.Data.DataLoader((train_x, train_y), batchsize=batch_size, shuffle=shuffle)
end

# This is a manual training loop, but it could be simpler using Flux.train!
function train!(epochs)
    @info "start training"
    for i = 1:epochs
        # Create batches from data
        for (x,y) in get_dataloaders(32, shuffle=false)
            #x, y = todevice(x, y) #move to gpu
            grad = Flux.gradient(()->copy_objective_batch(x, y), trainable_params)
            if i % 8 == 0
                l = copy_objective_batch(x, y)
                @info "Loss = $l"
            end
            # update parameters
            Flux.update!(optimizer, trainable_params, grad)
        end
    end
end


    # NOTE: this should be equivalent to the following,
#yes but does not print intermediate loss function, I tried to experiment with callback printing or plotting loss but did not succeed.
function train2!(epochs)
    train_loader = get_dataloaders(32, shuffle=false)
    Flux.@epochs epochs Flux.train!(copy_objective_batch, trainable_params, train_loader, optimizer)
end
# NOTE: there's a mismatch with the data copy_objective needs and the data loader
# I think the `copy_objective` must produce results for an entire minibatch
#for (x,y) in get_dataloaders(32, shuffle=false)
#    @info "data size" size(x) size(y)
#    @info typeof(x)
#    @info "loss" copy_objective_batch(x,y)
#end
