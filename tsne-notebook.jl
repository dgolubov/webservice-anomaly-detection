### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 449ee541-6b1d-41ff-99b7-95cc72eecb9f
begin
	import Pkg
	Pkg.activate(".")
	using TSne
	using Random
	using Plots
	using Statistics
	using StatsBase
	using LinearAlgebra
	using Plots
	using FileIO
	using DataFrames
	using TimeSeries
	using Chain
	using Printf
	#Pkg.add("DynamicAxisWarping")
	#using DynamicAxisWarping
end

# ╔═╡ 824a19f7-09a5-4610-8ac1-896c6c2bffd4
md"# Notebook for testing t-SNE
t-SNE is a dimension reduction method for visualising high dimensional data in lower dimensions, typically 2 or 3. The method can also be used as unsupervised learning as clustering, as it tries to preserve clusters found in the original data. We try examples such as two standard normals with mean 0 and 2 respectively. The first step is to load all necessary packages.
"

# ╔═╡ 65dbd946-e4a5-4230-863d-8e770b349327
md"We set a random seed to get reproducible results."

# ╔═╡ e96dd7b5-ac5b-408f-b76b-77241fe9891e
g = Random.seed!(1234)

# ╔═╡ fae45767-74a4-4250-aa0d-e8fe8f610f41
md"We generate 100 10-dimensional standard normal variables."

# ╔═╡ b4c8ed0d-7582-4bdf-a295-bd758f0085c9
X1 = randn(g, (100,10))

# ╔═╡ 0168ce55-9f7f-41c0-8bce-3047aae32c09
md"We generate 100 10-dimensional normal variables, this time with mean 2."

# ╔═╡ 56303a51-c03d-4360-91ec-13381551d4bd
X2 = 2 .+ randn(g, (100,10))

# ╔═╡ 829dfdab-2a4f-4cab-9bdd-b2f417c0aaff
md"We combine these two."

# ╔═╡ 83e67e2e-56e4-416e-be22-7f264faf6204
X = vcat(X1,X2)

# ╔═╡ 91576546-8378-4037-ae6d-8a8c0f699d6d
md"Then we assign them labels."

# ╔═╡ 57965581-1f55-43f9-8b4b-2f036c7db6b0
lab = vcat(repeat([0],100,10),repeat([2],100,10))

# ╔═╡ 07a6d5eb-a8ef-4389-80e9-deba617db980
md"Now we run a 2D t-SNE and plot it."

# ╔═╡ 6b8474aa-4f69-4c36-abbd-2cbf0690c553
Y_2D = tsne(X, 2, 0, 1000, 5);

# ╔═╡ 5e2f2ae8-9de4-4251-abc9-5a2ba499c8e2
plot2D = scatter(Y_2D[:,1], Y_2D[:,2],color=Int.(lab[1:size(Y_2D,1)]))

# ╔═╡ 659ac62c-1623-4201-9ed9-15a357e52ed7
md"We do the same thing but now in 3D."

# ╔═╡ 9a40e4b2-e69e-4bcf-914b-f7eeb29bb6b1
Y_3D = tsne(X, 3);

# ╔═╡ 4e344f67-e21c-4e03-af38-f7328a482186
plot3D = scatter(Y_3D[:,1], Y_3D[:,2], Y_3D[:,3], color=Int.(lab[1:size(Y_2D,1)]))

# ╔═╡ 0a182dad-42b6-4ce5-a371-d1da2345d438
md"# EROS 
Now for the next example, this time concerning MTS objects and t-SNE with the EROS similarity metric."

# ╔═╡ a1e74559-f620-483b-86d0-06885d6b94ec
md"The weight vector from eigenvalues for scaling in Algorithm 1 in the paper [A PCA-based Similarity Measure for Multivariate Time
Series - Kiyoung Yang and Cyrus Shahabi](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.106.7999&rep=rep1&type=pdf)"

# ╔═╡ 2f2828d0-d4f0-45a7-8a48-f3a6f1995341
function computeWeightRaw(S)
    n = size(S)[2]
    w = Vector{Float64}(undef,n)
    for i in collect(1:n)
        w[i] = mean(S[:,i])
    end
    replace!(w, NaN => 0)
    w = w./sum(w)
end

# ╔═╡ 56850472-c9c9-484f-8050-236bbdc3c97a
md"The EROS similarity metric given weight vector and eigenvectors as described in [m-TSNE: A Framework for Visualizing High-Dimensional
Multivariate Time Series](https://arxiv.org/abs/1708.07942).
"

# ╔═╡ 8328c37f-229d-43f9-8f5a-8b875dfe88c9
function EROSindex(i,j,v,w) 
    eros = 0.0
	n = length(w)
    for l in collect(1:n)
        eros += w[l]*abs(dot(v[i,:,l],v[j,:,l]))
    end
    eros
end

# ╔═╡ c0cad53c-89af-4015-a64e-04825e973f60
function EROS_sim(mts)
	n = minimum(size(mts[1]))
	N = length(mts)
	S = zeros(Float64, (N,n));
	v = zeros(Float64, (N,n,n));
	for k in eachindex(mts)
		A = mts[k]
		B = StatsBase.cov(A)
		F = LinearAlgebra.svd(B)
		S[k,:] .= F.S
		v[k,:,:] .= F.V
	end
	#w = computeWeightRaw(S)
	w = ones(n)
	dist_mat = zeros(Float64,(N,N));
	for i in collect(1:N)
		for j in collect(1:N)
			dist_mat[i,j] = EROSindex(i,j,v,w)
		end
	end
	dist_mat
end

# ╔═╡ 3a86c310-d8ea-4c2c-be5d-aa9e1c595ca6
function load_data(filename) 
    df = @chain filename joinpath("data/prometheusmetrics/webeos", _) load(_)["ts"]
    df[isnan.(df."error rate (15m)"), :"error rate (15m)"] .= 0 #set NaN error rates to zero
    err = df[!,"error rate (15m)"]
    m = maximum(err[err .!= Inf])
    df[isinf.(df."error rate (15m)"), :"error rate (15m)"] .= 10*m #set Inf error rates to 10max
    df
end

# ╔═╡ f30d9044-9b04-4224-9db8-0ea91e6fc4ee
lhcdf = load_data("lhcbproject.jld2")

# ╔═╡ a038d402-5d48-4fb0-a263-331609825eed
ts = TimeArray(lhcdf, timestamp = :time)
lossval
# ╔═╡ a5af3276-0075-44f0-b1b3-9f120169c9bd
mat = values(ts)

# ╔═╡ f5e4ec1c-e0bf-40ca-a01c-f118c46d0625
function featureNormalize(X)
    mu = mean(X,dims=1)
    sigma = std(X,dims=1)
    
    X_norm = (X .- mu) ./ sigma
    
    return X_norm
end

# ╔═╡ 7ac8ff2e-24fd-49d3-a837-a5f7cfb9d79a
mat2 = featureNormalize(mat)

# ╔═╡ 791a557d-5a7a-41ca-84d0-eeaa854757bd
md"# Sine example"

# ╔═╡ f8c44c40-fece-421c-84e6-cfb8a1610a37
BM = cumsum(X1,dims = 1)

# ╔═╡ 2b6e877f-df83-40ad-aade-bb55d17c86e3
function segment(M,inc)
	[M[i - inc + 1:i,:] for i in collect(inc:inc:size(M)[1])]
end

# ╔═╡ 6934dbef-a804-4fe2-be15-c877f4a191a9
mts = segment(mat,15)

# ╔═╡ 3c93df63-8b3c-470a-80f3-80f6de075ef0
mts2 = segment(mat2,15)

# ╔═╡ 56774ec6-128f-47cc-82b5-b440bd3d9f65
dist_mat = EROS_sim(mts2)

# ╔═╡ 1c9d1566-4f48-4cfb-bad7-89686971765d
heatmap(dist_mat)

# ╔═╡ 9d9f8069-9c16-4cad-ab74-9d80947865f2
tmts = tsne(dist_mat,2, 0, 1000, 30,distance = true)

# ╔═╡ f166f325-3cbc-4674-b83e-a9b8da1a337a
scatter(tmts[:,1],tmts[:,2],color = Int.(round.([sum(mts[i]) for i in collect(1:size(dist_mat)[1])])))
lossval
# ╔═╡ bb6aab13-8a9e-42e5-9da4-419803cdb2f4
T = collect(0:0.1:31.4)

# ╔═╡ aea8972d-095a-422e-9545-edef89d14164
Sin1 = sin.(T)

# ╔═╡ 9af06846-5ea8-4b98-8236-af875ae4defd
plot(Sin1)

# ╔═╡ 0b929534-7a33-4749-a107-5be18e23fb9d
Sin2 = sin.(2*T)

# ╔═╡ f536564f-be06-48ad-9b59-982ccc441dae
Sin3 = sin.(3*T)

# ╔═╡ 918c3211-682b-40f4-9a89-66f7b643c125
Sin4 = sin.(4*T)

# ╔═╡ 5f7306b2-2e2a-4553-a7e1-1aaaba569eblossvald
Sin = hcat(Sin1,Sin2) 

# ╔═╡ f6266ad7-9284-4d66-a3c5-0eb5fcf6c679
Noise = vcat(0.001*randn(g,(Int.(0.75*length(T)*100),2)),randn(g,(Int.(0.25*length(T)*100),2))) # #zeros(Int.(0.75*length(T)*100),4)

# ╔═╡ 12cea3d7-7d44-4797-9514-4564337306b5
Sin_rep = repeat(Sin,100) + Noise

# ╔═╡ 19749ec4-4b18-4ebd-a656-127c7833cd07
plot(Sin_rep[:,1])

# ╔═╡ dad07ad6-c18b-4c9a-b21d-a3c4e75301a0
SinMTS = segment(Sin_rep,315)

# ╔═╡ d055460b-2852-4f42-91c2-086efed32c0b
begin
	n = minimum(size(SinMTS[1]))
	N = length(SinMTS)
	S = zeros(Float64, (N,n));
	v = zeros(Float64, (N,n,n));
	for k in eachindex(SinMTS)
		A = SinMTS[k]
		B = StatsBase.cov(A)
		S[k,:] .= eigvals(B)
		v[k,:,:] .= eigvecs(B)
	end
	w = computeWeightRaw(S)
	v[1,:,:]-v[2,:,:]
end

# ╔═╡ 3fabad52-e526-4bcf-a611-603dd69f7aab
SinMTS[1] - SinMTS[2]

# ╔═╡ 21d3b087-f073-491f-8e81-2aef1b88549b
begin
	A = SinMTS[2]
	B = StatsBase.cov(A)
	F = svd(B)
end

# ╔═╡ 23e50472-1a7f-4515-a5e4-67ee1013da3f
plot(SinMTS[1][:,1]-SinMTS[2][:,1])

# ╔═╡ 98798424-4799-4b8e-8b2a-7ebc0a9fb459
sum(SinMTS[1]-SinMTS[2])

# ╔═╡ f666674a-2eb7-4692-bdcf-b67bcb6537b5
sim_mat = EROS_sim(SinMTS)

# ╔═╡ 99f772b6-0f57-4a9a-bdc9-9a9b8e95ead7
heatmap(sim_mat)

# ╔═╡ da9e0ab3-dc6b-4e2e-ac41-60d68ea889c7
div_mat = 1 ./ sim_mat

# ╔═╡ 85a1cc65-d4b1-4a44-8cd3-eddefed438cc
heatmap(div_mat)

# ╔═╡ c5fb9f9c-b370-481a-966f-5c3b21827e93
t = tsne(sim_mat,2, 0, 1000, 30,distance = true)

# ╔═╡ 0a3d5c77-8cef-4520-8fc4-cb3fbe2025ec


# ╔═╡ 0d13656f-b6f5-4f33-a28e-4b39f2596a4b
scatter(t[:,1],t[:,2],color = Int.([i < round(0.75*size(t)[1]) for i in collect(1:size(t)[1])]))

# ╔═╡ 8c90e2ad-51c1-4d29-8c83-924e04c29e0e
t2 = tsne(div_mat,2, 0, 1000, 30,distance = true)

# ╔═╡ 442dc243-bb65-43cf-8e33-153fdc95dbb8
scatter(t2[:,1],t2[:,2],color = Int.([i < round(0.75*size(t2)[1]) for i in collect(1:size(t2)[1])]))

# ╔═╡ Cell order:
# ╟─824a19f7-09a5-4610-8ac1-896c6c2bffd4
# ╠═449ee541-6b1d-41ff-99b7-95cc72eecb9f
# ╟─65dbd946-e4a5-4230-863d-8e770b349327
# ╠═e96dd7b5-ac5b-408f-b76b-77241fe9891e
# ╟─fae45767-74a4-4250-aa0d-e8fe8f610f41
# ╠═b4c8ed0d-7582-4bdf-a295-bd758f0085c9
# ╟─0168ce55-9f7f-41c0-8bce-3047aae32c09
# ╠═56303a51-c03d-4360-91ec-13381551d4bd
# ╟─829dfdab-2a4f-4cab-9bdd-b2f417c0aaff
# ╠═83e67e2e-56e4-416e-be22-7f264faf6204
# ╟─91576546-8378-4037-ae6d-8a8c0f699d6d
# ╠═57965581-1f55-43f9-8b4b-2f036c7db6b0
# ╟─07a6d5eb-a8ef-4389-80e9-deba617db980
# ╠═6b8474aa-4f69-4c36-abbd-2cbf0690c553
# ╠═5e2f2ae8-9de4-4251-abc9-5a2ba499c8e2
# ╟─659ac62c-1623-4201-9ed9-15a357e52ed7
# ╠═9a40e4b2-e69e-4bcf-914b-f7eeb29bb6b1
# ╠═4e344f67-e21c-4e03-af38-f7328a482186
# ╟─0a182dad-42b6-4ce5-a371-d1da2345d438
# ╟─a1e74559-f620-483b-86d0-06885d6b94ec
# ╠═2f2828d0-d4f0-45a7-8a48-f3a6f1995341
# ╟─56850472-c9c9-484f-8050-236bbdc3c97a
# ╠═8328c37f-229d-43f9-8f5a-8b875dfe88c9
# ╠═c0cad53c-89af-4015-a64e-04825e973f60
# ╠═3a86c310-d8ea-4c2c-be5d-aa9e1c595ca6
# ╠═f30d9044-9b04-4224-9db8-0ea91e6fc4ee
# ╠═a038d402-5d48-4fb0-a263-331609825eed
# ╠═a5af3276-0075-44f0-b1b3-9f120169c9bd
# ╠═f5e4ec1c-e0bf-40ca-a01c-f118c46d0625
# ╠═7ac8ff2e-24fd-49d3-a837-a5f7cfb9d79a
# ╠═6934dbef-a804-4fe2-be15-c877f4a191a9
# ╠═3c93df63-8b3c-470a-80f3-80f6de075ef0
# ╠═56774ec6-128f-47cc-82b5-b440bd3d9f65
# ╠═1c9d1566-4f48-4cfb-bad7-89686971765d
# ╠═9d9f8069-9c16-4cad-ab74-9d80947865f2
# ╠═f166f325-3cbc-4674-b83e-a9b8da1a337a
# ╟─791a557d-5a7a-41ca-84d0-eeaa854757bd
# ╠═d055460b-2852-4f42-91c2-086efed32c0b
# ╠═3fabad52-e526-4bcf-a611-603dd69f7aab
# ╠═21d3b087-f073-491f-8e81-2aef1b88549b
# ╠═f8c44c40-fece-421c-84e6-cfb8a1610a37
# ╠═2b6e877f-df83-40ad-aade-bb55d17c86e3
# ╠═bb6aab13-8a9e-42e5-9da4-419803cdb2f4
# ╠═aea8972d-095a-422e-9545-edef89d14164
# ╠═9af06846-5ea8-4b98-8236-af875ae4defd
# ╠═0b929534-7a33-4749-a107-5be18e23fb9d
# ╠═f536564f-be06-48ad-9b59-982ccc441dae
# ╠═918c3211-682b-40f4-9a89-66f7b643c125
# ╠═5f7306b2-2e2a-4553-a7e1-1aaaba569ebd
# ╠═f6266ad7-9284-4d66-a3c5-0eb5fcf6c679
# ╠═12cea3d7-7d44-4797-9514-4564337306b5
# ╠═19749ec4-4b18-4ebd-a656-127c7833cd07
# ╠═dad07ad6-c18b-4c9a-b21d-a3c4e75301a0
# ╠═23e50472-1a7f-4515-a5e4-67ee1013da3f
# ╠═98798424-4799-4b8e-8b2a-7ebc0a9fb459
# ╠═f666674a-2eb7-4692-bdcf-b67bcb6537b5
# ╠═99f772b6-0f57-4a9a-bdc9-9a9b8e95ead7
# ╠═da9e0ab3-dc6b-4e2e-ac41-60d68ea889c7
# ╠═85a1cc65-d4b1-4a44-8cd3-eddefed438cc
# ╠═c5fb9f9c-b370-481a-966f-5c3b21827e93
# ╠═0a3d5c77-8cef-4520-8fc4-cb3fbe2025ec
# ╠═0d13656f-b6f5-4f33-a28e-4b39f2596a4b
# ╠═8c90e2ad-51c1-4d29-8c83-924e04c29e0e
# ╠═442dc243-bb65-43cf-8e33-153fdc95dbb8
