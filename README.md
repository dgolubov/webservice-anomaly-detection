# webservice-anomaly-detection
<a href="https://summerstudent.web.cern.ch/home" target="_blank"><img height="45px" src="https://img.shields.io/badge/CERN%20Summer%20Student-2021-5c5c5c?style=flat&labelColor=0033A0&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAAPFBMVEVHcEz///////////////////////////////////////////////////////////////////////////+PybD1AAAAE3RSTlMAJkI02ugG+vLHDoyzoBlRXm18cLn1dwAAB1BJREFUeNqsVoea4ywMDL2b9v7v+gcEkU0gf7u5211/FI3KSPbrN+RFGI0x5Y4UV9A72A0EcV0/CCxLzhsutDpD3yFu4HccGagzuv4h7CmuDAx/CHsKXm/oSVH1gZkO8YC+QyE2tU6mTpOae5cjZYzG7DzH6HgmVwO5gz1AEV8cJHwYTKGXfCghezE3A3n9Z0Q+fXXMbsIkxYATisf/SCGznuk4itumQaOL/W3scN/BdVGA4kxTO8KPYzR4n3aXAyTCs7P9i7SKThZ/Lkyz5eWJQ2S7j57E0gdARwV4e8qV35JIB9Wg2ztNWKp+Qa3ymLAt1vC1XLoNw3Y3YhD1AO23GrmaSt26mnTnICc5IdQbFXCWImlelWWR8RMH9R+TSphQUqQ0phLuA8Aku9prm2nx1neX2HfYbprSxkVi768aWkBkO0VS9V5c2jV3M+k7jGlGODoJVn8BPD2klJq9JzHhFeq0IIlhoZC9UN1daHbxWpCvzqnmWk0UPaI49tw1I10HQGPnD4NMtODiYcgY+jqD6h6pxgGAbhv7FUiQKwfU2/2cYxKSkPjCsjY8aFqwbbpFOhNgFlQkHliuU8OXHshOODhkznBdwa8rQF0skCwND6ya7lp2aZxzVQSb89XJj1zLeup7nEa+cJznLRiEhlZpbXjMVv6+TeI/e40nNZQEeuZk0/DWY99YWsIbLjFLc04NOUfWHnPOici3yVBmcdveHLiagQiGTKMaS5j8qbb4GbfCd9HAs6kDwlnLh4TAW34NFUMBshoNtzY8VSAE/IpYoDk+u+42sEQ1DOU6iyr9eCxLw+eu8+lGVdx0LoUkpv/uC4KZj1AjRAJVBrrZNS+3NLyDE5C2auJlWVCVi97IOReXQztBY089NaMdMF1QlKElB6EsnxHSj8gcvrVkcrEZ849h5yYJsHTpG0xSwApr6p/tLQ2QWjNFjsuGxpQYkNAMIjKzOpJ1EhziFkMJBhsep8yIGWsFnEq1y/3emLM42x2SFCjXbaLrR8OD8YBKRhIAkAwY0iJUoAzfSCRopyFh+jvSQuIGidmQcCTxpK+LPEQ9vUoQ2m0AgMZWEvgryKMm3LngM6QZqg0kJApYwEiAEF/lz4a/Rk3AdTcoGOvLqC6tYdvyfj1pjARIsKAjX4IshfdyVi9J+A4SGkiktD1dvOiuvkECLDMSCFVT7G7kxEIZO8PUIdFsZjmC9+aN7pcDJ/ibio75gBLuyPi6W9/wYYY2Eq1U3UAQEK/Qs6RFTUtyNuityvWvTq1EOVYQCJaAIHKz//+vLxun2mPUMK/rjOXaMve0OXf4jpPW+QPsMyjcqHCpYAjwK56Kd7cRwQbU/aQ60mHmpPyH5Aa3GOfctxUFt4GMbxM9V7mDq+Gizno0ndTq2kPv35aoi9pQ1FRU0d8Y+f2rFGXRtRQS/pAZ/ZqLrJ7Ised4h23AiHvOl9bVChgw15jd18VdchF1E/Ov914NEyAvQtrtoRy3TYXT9qlsFKjXpe1vrxDcAM0Ync2XA4BxzPr75LZfSexVcDxUGaHbw1YwAkzDrIG43quM4CA0pdIPEcKVHRiTH6qMwCNkm4AfJnIPi0EsKKgyY2jovHXLBOI1+T6blnKsMgNA0AQU334eSdmCQgbTiyC81rgbeqV03Kzfb1YMyDHDVQaFGHNUwgBHduM7LDaYRPcMoLmjUtQ3f2fuErwEWOpn1F51gbGQ6BWpyFChzQQ7zYNBXOc9XBCkaXvh+lBIiSZlrBvPgJKAGzHZ6vl4Ol7uN6b41QjcX67PpOym635AVYaDnEaiz+L+TpVCoeLzxejOIZnvgzhgZL8/MTCFBTfdaUbJPgsxfp3iBwiPluoQqBWXBN+qUtpKUCbRBpbgiicoPvM1m+LrQfaSZauBGNf0emGYKg1N0OCYZ7lKc7doT2nBq/pQ1ZTXH+RJ10AMG1y7L8muvzc4M0HaBJbZ+B+YGdTE0ZlRtGEz8N1RCzGWeH6k+zimOjOOyX/eSTBIwr/+QWsmnfuJ470irR4Rha3sDLeY2EtEoj/orq8VPJ0j3PaNxYQfpBB6KzpbG9wtBz7uvJc93tA2FhdxfDDH/CBRRwyrb0HsV/5ZyDRcfOCATJFJ3ngP4lnfaM/Ol3XvhO4q6Ou0QH3+c9Zp3GvaU0zFst5/mLA6UIbOjYaI1wKujXOz4sam0/T2KzMZfSozSCivDt3xjUUpba9GLOeEAceaVYt7rQz5kAjSr3ao7NANQ2+th+QPq+sSFdulpZgCaM6Aw+zuWFQnOU16pDBJWdZi/4/E38oIi0n1YprM01HEsfTSk98+jxvjY2gKacfURjFyRJOyds3b/z087ciBp6OAo4/YAOkoxJrQpEaAdJRzfEY5kI4ijoAmNQRsjAJYGQfqbJVwoIGMQ5qOtsk5xOnY0EAEsFFUI6uYA+noRtOxzGggImAikXBIocfTURlsIEJA9hi4ExuIFJZ2nSESBw4hqnNY9N6hW2tKToAPkMdE+QcS1+0WlPAXfQAAAABJRU5ErkJggg==" /></a>

A project in detecting anomalies in CERNs web service metrics. The aim of the project was to detect error rate or rapid changes in latency by time series analysis and machine learning techniques, like LSTMs and Transformers.

Relying on haproxy logs/metrics, model backend services and detect anomalous states.

ToC:
- [K8s cluster setup](docs/cluster_setup.md)
- [Instructions for working environment](docs/working_env.md)

# tsne-notebook.jl
A notebook for running tsne visualisation with euclidean distance and EROS respectively. It includes some test examples and also our current data. The EROS similarity metric has some errors as of now. 

# websitelist.jl
Loading the significant websites which is the result from the filtering of `src/loadmetrics.jl`. This is the notebook to run for seeing the plots of the most interesting websites.

# scripts
Includes `julia` scripts. The `Example.jl` file is for running TPA-LSTM network on our data. The `loadmetrics.jl` is the script for loading the data from local folder `webeos` and storing the relevant websites in `data.jld2`. The `notebook.jl` is for plotting the data compared to the sample data in TPA-LSTM. 

The transformer related files are `attention_model.jl`, `transformer_model.jl`, `mts_transformer_example.jl` which are all non-relevant example files. Our used files are `timeseries_transformer.jl` and `gated_transformer.jl`.

# src 
Library files that can be run from scripts. The file `dataloader.jl` contains a function for getting website data ready for the machine learning in `Example.jl` and for the transformer model files. Transformer files are `attention_data.jl`, `transformer_data.jl` which are example files for trying the transformer model. Our currently used transformer files are `trainwithprogress.jl` and `transformer_model.jl`. The `loadfile.jl` file is for loading the data. It should be called from some other file and be given parameters for the path of the file. The `statistics/plotting.jl` and `statistics/tsnetest.jl` are now obsolete since they are run in the `websitelist.jl` and `tsne-notebook.jl` Pluto notebooks instead, respectively. 

# tmp-plots
Temporary plots that are ignored in the git repo.

# report
The summer project report, where `main.pdf` is the report file.
