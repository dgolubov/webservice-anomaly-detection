cd(joinpath(@__DIR__, "..")); using Pkg; Pkg.activate("."); Pkg.instantiate();
using ArgParse
using YAML
using Random
using Flux, BSON, GR, TimeSeries, SliceMap, JuliennedArrays
Random.seed!(0)

include("../src/dataloader.jl")

function parse_commandline()
  s = ArgParseSettings()

  @add_arg_table s begin
    "--gpu", "-g"
    help = "use gpu"
    action = :store_true
    
    "task"
    help = "task name"
    required = true
    range_tester = x-> x ∈ ["webeos/cvmrepo.jld2", "copy"]
  end

  return parse_args(ARGS, s)
end

const args = parse_commandline()

enable_gpu(args["gpu"])

function preprocess(data)
    x, t = data
    x = mkline.(x)
    t = mkline.(t)
    x_mask = getmask(x)
    t_mask = getmask(t)
    x, t = vocab(x, t)
    todevice(x,t,x_mask,t_mask)
end

if args["task"] == "copy"
  const N = 2
  const V = 10
  const Smooth = 1e-6
  const Batch = 32
  const lr = 1e-4

  startsym = 11
  endsym = 12
  unksym = 0
  labels = [unksym, startsym, endsym, collect(1:V)...]

  function gen_data()
    global V
    d = rand(1:V, 10)
    (d,d)
  end

  function train!()
    global Batch
    println("start training")
    model = (embed=embed, encoder=encoder, decoder=decoder)
    i = 1
    for i = 1:25
      data = batched([gen_data() for i = 1:Batch])
      x, t, x_mask, t_mask = preprocess(data)
      grad = gradient(ps) do
          l = loss(model, x, t, x_mask, t_mask)
          l
      end
        i%8 == 0 && @show loss(model, x, t, x_mask, t_mask)
      update!(opt, ps, grad)
    end
  end

  mkline(x) = [startsym, x..., endsym]
else
  const N = 6
  const Smooth = 0.4
  const Batch = 8
  const lr = 1e-6
    
  startsym = 11
  endsym = 12
  unksym = 0
  labels = [unksym, startsym, endsym, collect(1:7)...]

  const file = args["task"]
  poollength = 15  
  horizon = 5  
  datalength = 100
  parameters= YAML.load_file("parameters.yaml")
  data = [get_website_data(file, poollength, datalength, horizon, batch) for batch in collect(1:Batch)]

  function train!()
    global Batch
    println("start training")
    model = (embed=embed, encoder=encoder, decoder=decoder)
    i = 1
    for i = 1:25
      batch = data[i%8+1]
      x, t, x_mask, t_mask = preprocess(batch)
      grad = gradient(ps) do
          l = loss(model, x, t, x_mask, t_mask)
          l
      end
        i%8 == 0 && @show loss(model, x, t, x_mask, t_mask)
      update!(opt, ps, grad)
    end
  end
  mkline(x) = [startsym, x..., endsym]
end