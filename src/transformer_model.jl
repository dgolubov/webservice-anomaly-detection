
"""
    LinearPositionEmbedding(metrics, width)

Mix a Dense layer and a position encoder to produce Transformer input.
Given an input timeseries segment `x: {time} × {metrics}`, it applies the Dense layer on each sample of `x` (along the metrics).

    y = linearPositionEmbedding(x)

Dimensions:
- x: {time} × {metrics}
- y: {width} × {time}
"""
struct LinearPositionEmbedding
    linear::Flux.Dense
    pe::Transformers.PositionEmbedding
end
LinearPositionEmbedding(metrics, width)= LinearPositionEmbedding(Flux.Dense(metrics, width), PositionEmbedding(width))
(l::LinearPositionEmbedding)(x)= @chain x begin
    permutedims([2,1,3])
    l.linear
    _ .+ l.pe(_)
end
Flux.@functor LinearPositionEmbedding

onehotbatch(target, targetvocab, unknownLabel)= Flux.onehotbatch(target, targetvocab, unknownLabel)
onehotbatch(target::CuArray, targetvocab, unknownLabel)= Flux.onehotbatch(target, targetvocab, unknownLabel)|> gpu

"""
    encodeDecodeLoss(x, target)

Loss function for predicting with a probabilistic Decoder, receiving the same input as a paired encoder.
The decoder's output is compared with the distribution of the target across the vocabulary.
The target must be shifted accordingly a priori (the data loader should prepare `train_y` that are shifted as necessary).
"""
function encodeDecodeLoss(x, target)
    enc = encoder(x)
    predictionDistribution = decoder(x,enc)
    targetDistribution= onehotbatch(target, targetvocab, unknownLabel)
    Flux.logitcrossentropy(predictionDistribution, targetDistribution)
end

"""
    decodeLoss(x, target)

Loss function for predicting with a probabilistic Decoder, that doesn't have a paired encoder.
The decoder's output is compared with the distribution of the target across the vocabulary.
The target must be shifted accordingly a priori (the data loader should prepare `train_y` that are shifted as necessary).
"""
function decodeOnlyLoss(x, target)
    predictionDistribution = decoderSingle(x)
    targetDistribution= onehotbatch(target, targetvocab, unknownLabel)
    Flux.logitcrossentropy(predictionDistribution, targetDistribution)
end

"""
    y = distributionModeBatch(x)

Given a probability distribution for a sample minibatch, get the label (vocabulary index) with the highest probability,
for each time and each sample in the batch. Drops singleton dimensions.
- x: {labels} × {time} × {batch} # {labels} × {time} × {1} × {batch}
- y: {time} × {batch}
"""
#I had to change this function due to dimension issues
distributionMode(x)= @chain x begin
    argmax(dims=1)
    getindex.(1)
    dropdims(dims=1) #dropdims(dims=(1,3)
end

"""
    total_plot(data)

Plotting all the data from all batches. unfinished concatenation function as of now
"""
function total_plot(preprocessedData,data,epoch,loss)
    newsize = size(preprocessedData,1)
    lenbatch = size(first(data)[1],3)
    concat_x = preprocessedData[:,6] |> f32
    concat_y = Vector{Float32}(undef,lenbatch*div(newsize,lenbatch))

    for (i,d) in enumerate(data)
        x, target = d
        predictionDistribution = decoderSingle(x)
        label = distributionMode(predictionDistribution)

        concat_y[(i-1)*lenbatch+1:i*lenbatch] = concat_batch(target)[15:end]
    end

    p = plot(concat_x[1:lenbatch*length(data)], xlabel = "Time", ylabel = "Response latency", title = @sprintf "Loss %f at epoch %i" loss epoch)
    plot!(concat_y)
    savefig(p, @sprintf "tmp-plots/prediction_total_epoch%i.png" epoch)
end

"""
    plot_prediction(x, target)

Plots the prediction compared with the actual target for one specific sequence (not entire batch).
"""
function plot_prediction(x,target)
    predictionDistribution = decoderSingle(x)
    label = distributionMode(predictionDistribution)
    concat_x = concat_batch(label)

    y = dropdims(target, dims=2)
    concat_y = concat_batch(y)

    p = plot(concat_x)
    plot!(concat_y)
    savefig(p,"prediction.png")
end

"""
    concat_batch(batchdata)
Takes input : {time} × {batch} and gives back {time}
"""
function concat_batch(batchdata)
    nseq = size(batchdata,1)
    nbatch = size(batchdata,2)
    newsize = nseq + nbatch - 1
    concatdata = Vector{Float32}(undef, newsize)
    for i in collect(1:nbatch-1)
        concatdata[i] = batchdata[end,i]
    end
    concatdata[nbatch:end] = batchdata[:,end]
    return concatdata
end