"Create a [`BatchIterator`](@ref)"
batchIterator(x,batchSize::Integer)= return BatchIterator(x,batchSize)

"""
    BatchIterator{T}

Inspired by PartitionIterator, batch iterator yields a batch of results from the enclosed iterator,
which must return an Array/CuArray of the same size, concatenated as an array with an extra dimension.

# Example

```
x= Iterators.repeated(rand(3))
b= BatchIterator(x,2)
first(b)  # 3×2 Matrix{Float64}
p= Iterators.PartitionIterator(x,2)
first(p)  # 2-element Vector{Vector{Float64}}

first(b) == reshape(first(p)|>Iterators.flatten|>collect, 3,2)  # true
```
"""
struct BatchIterator{T}
    c::T
    n::Int
end
#Base.eltype(::Type{BatchIterator{Array{T,N}}}) where {T,N} = Array{T,N+1}
Base.IteratorEltype(::Type{BatchIterator{T}}) where {T} = Base.EltypeUnknown()
Base.IteratorSize(::Type{BatchIterator{T}}) where {T}= Base.HasLength()

function Base.length(itr::BatchIterator)
    l = length(itr.c)
    return fld(l, itr.n)
end

struct IterationCutShort; end

function Base.iterate(itr::BatchIterator, state...)
    # This is necessary to remember whether we cut the
    # last element short. In such cases, we do return that
    # element, but not the next one
    state === (IterationCutShort(),) && return nothing
    i = 0
    y = iterate(itr.c, state...)
    y === nothing && return nothing
    v = batchArrayTypeof(y[1])(undef, size(y[1])..., itr.n)
    while y !== nothing
        i += 1
        v[ntuple(_->:,length(size(y[1])))..., i] .= y[1]
        if i >= itr.n
            break
        end
        y = iterate(itr.c, y[2])
    end
    i === 0 && return nothing
    return v, y === nothing ? IterationCutShort() : y[2]
end

batchArrayTypeof(::Array{T,N}) where {T,N}= Array{T,N+1}
batchArrayTypeof(::CuArray{T,N}) where {T,N}= CuArray{T,N+1}