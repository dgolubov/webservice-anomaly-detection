import LinearAlgebra: norm
include("loadfile.jl")
include("batchiterator.jl")

"""
    get_data(filename, poollength, datalength, horizon)

`SegmentLength` is the segment of time steps used used for forecasting,
`datalength` is the length of the time series that we are using for training,
and `horizon` determines the number of time steps that should be forecasted by the model.
"""
function get_website_data(file, segmentLength, datalength, horizon; normalise=true)

    df64 = load_website(file) #loading all the data into a dataframe
    df32= DataFrame()
    for p in propertynames(df64)
        if p != :time
             setproperty!(df32, p, getproperty(df64,p).|> Float32)
        end
    end
    #ts = TimeArray(df, timestamp = :time) #storing it in a time array instead
    inp_raw = Matrix(df32) #values(ts) #taking the values and leaving the timestamp
    #we normalise our data using min-max normalisation
    if normalise
        inp_raw = Flux.normalise(inp_raw, dims=1)
    end

    #change dimensions to tuple of arrays instead of array of tuples
    segments, target = segmentTimeseries_webeos_horizonforecast(inp_raw, segmentLength, horizon)

    p = Binomial(1,0.2) #0.2 chance of 1 and 0.8 chance of 0
    val = rand(p,length(segments))
    #if val is 0 in a certain batch then it should be added to the training set, else test set

    input_train, target_train= [segments[i] for i in 1:length(segments) if val[i] == 0], [target[i] for i in 1:length(target) if val[i] == 0]
    input_test, target_test= [segments[i] for i in 1:length(segments) if val[i] == 1], [target[i] for i in 1:length(target) if val[i] == 1]

    #Convert to dataloaders
    batchsize = datalength
    train_loader = Flux.Data.DataLoader((input_train, target_train), batchsize=batchsize, shuffle=true)
    test_loader = Flux.Data.DataLoader((input_test, target_test), batchsize=batchsize, shuffle=true)

    return train_loader, test_loader
end

# Input dims
# - timeseries: {"timesteps" × "metrics"}
# Output dimensions:
# - Vector{"samples"}{"metrics" × "segment length" x "extra dim for LSTM"}
# - Vector{"samples"}
# Output ready to be batched
function segmentTimeseries_webeos_horizonforecast(timeseries, segmentLength, horizon)
    nSegments= size(timeseries,1) - segmentLength + 1 - horizon
    segments= Vector{Array{Float32, 3}}(undef, nSegments)
    target= zeros(nSegments)
    for i in 1:nSegments
        input = similar(timeseries[i : i+segmentLength-1, :], variables, segmentLength, 1)
        input[:,:,1] = timeseries[i : i+segmentLength-1, :]'
        segments[i]= input
        target[i]= timeseries[i+segmentLength-1 + horizon, 7]
    end
    segments, target
end

"""
    segmentTimeseries_shiftN(timeseries, segmentLength, targetMetric; shiftTarget=1)

Produces a source + target for timeseries predictions.
The source has time length {segmentLength}.
The target is the same segment of the time series as the source, only shifted by 1 to the future.
- `targetMetric`: the index of the timeseries metric that forms the target.

Input dims
- timeseries: {"timesteps" × "metrics"}
Output dimensions:
- generator{"samples"}{"metrics" × "segment length" x "extra dim for LSTM"}
- generator{"samples"}
Output ready to be batched
"""
function segmentTimeseries_shiftN(timeseries, segmentLength, targetMetric; shiftTarget=1)
    nSegments= size(timeseries,1) - segmentLength - shiftTarget +1
    segments= (timeseries[i : i+segmentLength-1, :] for i in 1:nSegments)
    targets= (timeseries[i+shiftTarget : i+segmentLength+shiftTarget-1, targetMetric] for i in 1:nSegments)
    segments, targets
end

"""
    segments,targets,vocabulary = dataloaderTransformerForecasting(websiteFile, segmentLength, quantizationLevelsPerMetric)

Prepare website data for forecasting. Clean, quantize, cut time series into segments.
For each segment return target (same segment shifted by 1).
Return also the vocabulary for each metric.
- targetMetric: the column name of the loaded dataframe corresponding to the metric we want to predict
"""
function dataloaderTransformerForecasting(websiteFile; targetMetric="response latency",
    segmentLength, batchSize, quantizationLevelsPerMetric)
    data= load_website(websiteFile) #loading all the data into a dataframe
    nMetrics = size(data,2) - 1
    targetMetric= findall(x-> x == targetMetric, data|>names)[1]

    quantizeData(data)= begin
        dataVocab= [quantize(data[!,m+1], quantizationLevelsPerMetric[m]) for m in 1:nMetrics]
        data= map(x->x[1], dataVocab)
        data= reshape(data|>Iterators.flatten|>collect, length(data[1]),:)
        data, map(x->x[2], dataVocab)
    end

    # NOTE: Since the reference doesn't do any quantization, nor any vocabulary, can we get a transformer to work without them too?
    # Consequence of no vocabulary: no probabilistic output

    # I added the pos_enc_timestamp now but they should not be used in the quantization so not sure how they are used now
    df = @chain data diffLatency capInfNan(nMetrics) pos_enc_timestamp
    q,vocab = quantizeData(df)
    @info "quantization error per metric" quantizationError.(eachcol(q),vocab)
    segments, target= segmentTimeseries_shiftN(q|>gpu, segmentLength, targetMetric-1, shiftTarget=5)

    # Return a lot of things
    (
        preprocessedData= q, vocabulary= vocab,
        train_loader= batchIterator(segments,batchSize),
        train_target_loader= batchIterator(target,batchSize),
    )
end

"Replace \"response latency\" column with its diff (which is sparse)"
diffLatency(data)= begin
    data[2:end,"response latency"].= data[:,"response latency"]|>diff
    data[1,"response latency"]= data[2,"response latency"]
    data
end
"Replace Inf & NaN with maximum value"
capInfNan(data, nMetrics)= begin
    for m in 2:(nMetrics+1)
        d= data[!,m]
        nanInf= isnan.(d) .| isinf.(d)
        max= maximum(data[(!).(nanInf), m])
        data[nanInf,m] .= max
    end
    data
end
"""
timestamp_data :: DataFrame = pos_enc_timestamp(df :: DataFrame)
Add the timestamps as new columns to the dataframe.
"""
function pos_enc_timestamp(df)
    timestamps = df[!,:time]
    df."week" = [Dates.week(t) for t in timestamps]
    df."day" = [Dates.day(t) for t in timestamps]
    df."dayofweek" = [Dates.dayofweek(t) for t in timestamps]
    df."hour" = [Dates.hour(t) for t in timestamps]
    df."minute" = [Dates.minute(t) for t in timestamps]
    return df
end

"""
    quantizedData, vocabulary = quantize(data,levels::Int)

Quantize numeric data to `levels` number of bins adapted to their distribution (like a histogram).
Return both the quantized data and the "vocabulary", ie upper bin limits.
"""
function quantize(data,levels::Int; minBinOccupancy= 2e-3)
    vocab= _vocabulary(data, levels, Val(sparsity(data) < 0.3), minBinOccupancy=minBinOccupancy)
    quantize(data,vocab), vocab
end

function _vocabulary(data, levels, isSparse::Val{false}; minBinOccupancy= 3e-3)
    histogram= fit(Histogram,data, nbins= levels)
    linBins, bincounts= (histogram.edges, histogram.weights).|>Iterators.flatten.|> collect
    mergedBins= [linBins[1]]
    scanStart= 1
    for i in 2:length(linBins)
        if sum(bincounts[scanStart:i-1]) > minBinOccupancy*length(data)
            push!(mergedBins, linBins[i])
            scanStart= i
        end
    end
    (mergedBins[end] != linBins[end]) && push!(mergedBins, linBins[end])
    mergedBins
end
_vocabulary(data, levels, isSparse::Val{true}; minBinOccupancy= 3e-3)= [2eps(); _vocabulary(denseNzval(data), levels, Val(false), minBinOccupancy=minBinOccupancy)]|> sort
denseNzval(x)= @chain x eltype zero x[.!(x .≈ _)]

"""
    quantizedData = quantize(data,vocabulary)

Quantize numeric data using the upper bin limits in `vocabulary`.
"""
quantize(data, vocabulary)= @chain data begin
    @aside binIdx= (a,b,i;leftclosed,rightclosed)->i|>string
    CategoricalArrays.cut(_, vocabulary, extend=true, labels=binIdx, allowempty=true)
    CategoricalArrays.unwrap.(_)
    parse.(Int,_)
end

"""
    quantizationError(d,vocab)

Quantization error of `q = quantize(d,vocab)`
"""
quantizationError(d,vocab)= norm(d - vocab[quantize(d,vocab)])

# Per column sparsity
sparsity(x::Matrix)= [1 - sum(x[:,n] .≈ 0) ./ size(x,1) for n in 1:size(x,2)]
sparsity(x)= 1 - sum(x .≈ 0) ./ length(x)
