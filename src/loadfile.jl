#using Pkg
#Pkg.activate("/home/jovyan/webservice-anomaly-detection")
using FileIO
#using JLD
using HDF5
using JLD2
using DataFrames
using StatsBase
using Distributions

function loading(filepath)
    try
        FileIO.load(filepath)
    catch e
       @error "Could not load file: $filepath"
    end
end

load_data(file)=loading(joinpath(parameters["datasetDir"], file))

time_series(dict::Nothing) = nothing
time_series(dict) = dict["ts"]

load_website(file) = time_series(load_data(file))