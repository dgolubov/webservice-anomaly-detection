### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 5fd1cb23-cfd5-468a-9523-b907ebc6809d
# Now we get gradients for each of the inputs `W`, `b` and `x`, which will come
# in handy when we want to train models.

# Because ML models can contain hundreds of parameters, Flux provides a slightly
# different way of writing `gradient`. We instead mark arrays with `param` to
# indicate that we want their derivatives. `W` and `b` represent the weight and
# bias respectively.

using Flux: params

# ╔═╡ eeb695fb-7f2d-4226-9657-62db80025003
# You don't have to use `train!`. In cases where arbitrary logic might be better suited,
# you could open up this training loop like so:

# ```julia
#   for d in training_set # assuming d looks like (data, labels)
#     # our super logic
#     gs = gradient(params(m)) do #m is our model
#       l = loss(d...)
#     end
#     update!(opt, params(m), gs)
#   end
# ```

# Training a Classifier
# ---------------------

# Getting a real classifier to work might help cement the workflow a bit more.
# [CIFAR10](url) is a dataset of 50k tiny training images split into 10 classes.

# We will do the following steps in order:

# * Load CIFAR10 training and test datasets
# * Define a Convolution Neural Network
# * Define a loss function
# * Train the network on the training data
# * Test the network on the test data

# Loading the Dataset

# [Metalhead.jl](https://github.com/FluxML/Metalhead.jl) is an excellent package
# that has a number of predefined and pretrained computer vision models.
# It also has a number of dataloaders that come in handy to load datasets.

using Statistics

# ╔═╡ 970f2eda-a2a9-4e05-b8cf-3f668fd0182f
using Images: channelview

# ╔═╡ bfb80a92-9a13-4ee9-8577-53b1d11824bc
using Metalhead

# ╔═╡ b74db7f3-03fd-42fe-8e89-e22c19e10a5d
using Metalhead: trainimgs, valimgs

# ╔═╡ ebb70637-1ecb-4dff-9a29-94f5e69f46dd
using Images.ImageCore

# ╔═╡ fc1a4d57-7d6d-46d2-867c-3c1df6f580b7
using Flux: onehotbatch, onecold, flatten

# ╔═╡ 50dbd980-b3f8-4289-a926-ec58fa48f640
using Base.Iterators: partition

# ╔═╡ d635c2a1-1324-4219-b671-6b519c7735b6
#-
# We will use a crossentropy loss and the Momentum optimiser here. Crossentropy will be a
# good option when it comes to working with multiple independent classes. Momentum smooths out
# the noisy gradients and helps towards a smooth convergence. Gradually lowering the 
# learning rate along with momentum helps to maintain a bit of adaptivity in our optimisation,
# preventing us from overshooting our desired destination.
#-

using Flux: Momentum

# ╔═╡ 6658db9f-6178-4f9b-b30e-633769648545
# Julia's arrays are very powerful, and you can learn more about what they can
# do [here](https://docs.julialang.org/en/v1/manual/arrays/).

# ### CUDA Arrays

# CUDA functionality is provided separately by the [CUDA
# package](https://github.com/JuliaGPU/CUDA.jl). If you have a GPU and CUDA
# available, you can run `] add CUDA` in a REPL or IJulia to get it.

# Once CUDA is loaded you can move any array to the GPU with the `cu`
# function, and it supports all of the above operations with the same syntax.

## using CUDA
## x = cu(rand(5, 3))

# Automatic Differentiation
# -------------------------

# You probably learned to take derivatives in school. We start with a simple
# mathematical function like

f(x) = 3x^2 + 2x + 1

# ╔═╡ 1ed3883f-91fa-4653-aa79-af916c454f32
f(5)

# ╔═╡ 123d36ad-ccf2-4395-8924-1da5b7faa93e
df(x) = gradient(f, x)[1]

# ╔═╡ 86c7af2d-8170-4db8-9ac8-31c23da3d738
df(5)

# ╔═╡ 494cb53e-f54b-4a9a-ab89-b1181f2d9058
# You can try this with a few different inputs to make sure it's really the same
# as `6x+2`. We can even do this multiple times (but the second derivative is a
# fairly boring `6`).

ddf(x) = gradient(df, x)[1]

# ╔═╡ ececa7d9-5f25-480d-84c1-b5768b235f84
I = collect(-2:0.1:2)

# ╔═╡ 68905575-c140-4b4a-9990-37c6e5b578d9
plot(I, f.(I))

# ╔═╡ ae800d10-4a23-4146-89e6-552650f3ef53
plot(I,df.(I))

# ╔═╡ b1eb6ba5-9d0b-4107-8780-05e8a7a329f6
plot(I,ddf.(I))

# ╔═╡ dd268469-21c0-4d73-9ad9-6bd40a1003c5
# Flux's AD can handle any Julia code you throw at it, including loops,
# recursion and custom layers, so long as the mathematical functions you call
# are differentiable. For example, we can differentiate a Taylor approximation
# to the `sin` function.

mysin(x) = sum((-1)^k*x^(1+2k)/factorial(1+2k) for k in 0:5)

# ╔═╡ cbad2052-d1c7-43b0-9734-2066c395f8ce
# You can see that the derivative we calculated is very close to `cos(x)`, as we
# expect.

# This gets more interesting when we consider functions that take *arrays* as
# inputs, rather than just a single number. For example, here's a function that
# takes a matrix and two vectors (the definition itself is arbitrary)

myloss(W, b, x) = sum(W * x .+ b)

# ╔═╡ 5827b3aa-4548-49ce-904a-82b8a30597d1
W = randn(3, 5)

# ╔═╡ 21456496-e04f-4a60-b912-dc1b85abce29
b = zeros(3)

# ╔═╡ 328e3bbd-dc7d-468d-b758-479a5f5e6fe9
x1 = rand(5)

# ╔═╡ 7c1017ae-4542-4b0b-9f9c-ceb5cf989484
y(x) = sum(W * x1 .+ b)

# ╔═╡ ddee81ba-e286-4a6e-a149-6284c5cb79f3
grads1 = gradient(()->y(x1), params([W, b]))

# ╔═╡ a1e44848-e309-4ee3-b562-e3e1076efcc9
grads1[W], grads1[b]

# ╔═╡ 6b993e29-8428-4bc8-94d5-0c1df5091170
m1 = Dense(10, 5)

# ╔═╡ ac000fe2-6b93-47f3-9daf-6cd803ede884
# We can easily get the parameters of any layer or model with params with
# `params`.

params(m1)

# ╔═╡ 9ceed1a3-f041-4b01-8d4c-74af7c76b6ab
# This makes it very easy to calculate the gradient for all
# parameters in a network, even if it has many parameters.
x = rand(Float32, 10)

# ╔═╡ bdab5d62-cdb7-4a9d-932a-f0915613a550
m2 = Chain(Dense(10, 5, relu), Dense(5, 2), softmax)

# ╔═╡ 2b88e19c-ac46-4d05-b2ab-6121339dd957
# using CUDA

# The image will give us an idea of what we are dealing with.
# ![title](https://pytorch.org/tutorials/_images/cifar10.png)

Metalhead.download(CIFAR10)

# ╔═╡ 75963a80-e623-4d45-9967-0c4580e310e5
X = trainimgs(CIFAR10)

# ╔═╡ 7fde1f5d-f662-478d-b7ee-a5d05b2f6f4d
# Let's take a look at a random image from the dataset

image(x) = x.img # handy for use later

# ╔═╡ ce116155-5152-4a05-a714-1c96f022cb2b
ground_truth(x) = x.ground_truth

# ╔═╡ b7b28239-e24e-4514-bc7b-1f9091ce1fb1
image.(X[rand(1:end, 10)])

# ╔═╡ a5e0fb39-34dc-47ca-a3a4-4ea3f9652545
# The images are simply 32 X 32 matrices of numbers in 3 channels (R,G,B). We can now
# arrange them in batches of say, 1000 and keep a validation set to track our progress.
# This process is called minibatch learning, which is a popular method of training
# large neural networks. Rather that sending the entire dataset at once, we break it
# down into smaller chunks (called minibatches) that are typically chosen at random,
# and train only on them. It is shown to help with escaping
# [saddle points](https://en.wikipedia.org/wiki/Saddle_point).

# Defining a `getarray` function would help in converting the matrices to `Float` type.

getarray(X) = float.(permutedims(channelview(X), (3, 2, 1)))

# ╔═╡ 250c9353-19e8-452a-ae90-bf6d251eb6d8
imgs = [getarray(X[i].img) for i in 1:50000]

# ╔═╡ 3c9b18f7-870b-4e0a-a5b2-e0da83351127
# ## Defining the Classifier
# --------------------------
# Now we can define our Convolutional Neural Network (CNN).

# A convolutional neural network is one which defines a kernel and slides it across a matrix
# to create an intermediate representation to extract features from. It creates higher order
# features as it goes into deeper layers, making it suitable for images, where the structure of
# the subject is what will help us determine which class it belongs to.

m = Chain(
  Conv((5,5), 3=>16, relu),
  MaxPool((2,2)),
  Conv((5,5), 16=>8, relu),
  MaxPool((2,2)),
  flatten,
  Dense(200, 120),
  Dense(120, 84),
  Dense(84, 10),
  softmax) |> gpu

# ╔═╡ c759656e-b6ad-4a3f-b784-3801e2946e00
l(x) = Flux.Losses.crossentropy(m(x), [0.5, 0.5])

# ╔═╡ 478ac041-a91a-4fda-a046-0e9498cd3afb
# We can start writing our train loop where we will keep track of some basic accuracy
# numbers about our model. We can define an `accuracy` function for it like so.

accuracy(x, y) = mean(onecold(m(x), 1:10) .== onecold(y, 1:10))

# ╔═╡ 7d180195-0508-4d81-9588-1c73b68fafe7
# ## Training
# -----------

# Training is where we do a bunch of the interesting operations we defined earlier,
# and see what our net is capable of. We will loop over the dataset 10 times and
# feed the inputs to the neural network and optimise.

epochs = 10

# ╔═╡ 3153c68e-977f-46d6-904b-f81e1260b079
valX = cat(imgs[valset]..., dims = 4) |> gpu

# ╔═╡ 1ae9fe36-3b80-4224-8220-ab4f0e9c73a5
valimg = [getarray(valset[i].img) for i in 1:10000]

# ╔═╡ 49dfe241-1a71-47d8-b5f4-fd128afe90e7
Flux.train!(loss, params(m), [(data,labels)], opt)

# ╔═╡ ea9a29cb-fa64-4573-973d-3acd47386cda
# The first 49k images (in batches of 1000) will be our training set, and the rest is
# for validation. `partition` handily breaks down the set we give it in consecutive parts
# (1000 in this case). `cat` is a shorthand for concatenating multi-dimensional arrays along
# any dimension.

train = ([(cat(imgs[i]..., dims = 4), labels[:,i]) for i in partition(1:49000, 1000)]) |> gpu

# ╔═╡ ae42ed84-6c60-48fc-9e9b-7a58ef934b98
valY = labels[:, valset] |> gpu

# ╔═╡ a370264f-b115-4c6a-8740-2a5a4197cd38
for epoch = 1:epochs
  for d in train
    gs = gradient(params(m)) do
      l = loss(d...)
    end
    update!(opt, params(m), gs)
  end
  @show accuracy(valX, valY)
end

# ╔═╡ f0c5c78c-da29-461b-af41-81f3b3f105bd
test = gpu.([(cat(valimg[i]..., dims = 4), labels[:,i]) for i in partition(1:10000, 1000)])

# ╔═╡ cc63f580-c61d-4e82-9045-547dbca1bf90
# Next, display some of the images from the test set.

ids = rand(1:10000, 10)

# ╔═╡ ca067855-e06b-4669-8920-5a141bc017ad
image.(valset[ids])

# ╔═╡ b7bd6f58-fb6a-49f6-9caf-a9eaa9dc8ce0
rand_truth = ground_truth.(valset[ids])

# ╔═╡ 39158790-94ae-4f0b-9591-5bcc647fa373
m(rand_test)

# ╔═╡ 1d49bcdc-b29a-454d-a903-eaa12be03da7
# This looks similar to how we would expect the results to be. At this point, it's a good
# idea to see how our net actually performs on new data, that we have prepared.

accuracy(test[1]...)

# ╔═╡ 7327657f-a6c0-4151-87d0-74380324f18e
# This is much better than random chance set at 10% (since we only have 10 classes), and
# not bad at all for a small hand written network like ours.

# Let's take a look at how the net performed on all the classes performed individually.

class_correct = zeros(10)

# ╔═╡ 2dbb3852-77b0-45be-a824-81b48e324a68
class_total = zeros(10)

# ╔═╡ 97843034-3187-4a87-a53f-1800956e742f
for i in 1:10
  preds = m(test[i][1])
  lab = test[i][2]
  for j = 1:1000
    pred_class = findmax(preds[:, j])[2]
    actual_class = findmax(lab[:, j])[2]
    if pred_class == actual_class
      class_correct[pred_class] += 1
    end
    class_total[actual_class] += 1
  end
end

# ╔═╡ 366a45f6-d80f-4ec3-ad5e-5226e03f00bd
class_correct ./ class_total

# ╔═╡ ea741139-6b55-4c59-8b4a-e0e4705a9180
# The spread seems pretty good, with certain classes performing significantly better than the others.
# Why should that be?

# ╔═╡ 3869a8db-c15c-4310-bff0-198300485feb
# `Training` a network reduces down to iterating on a dataset multiple times, performing these
# steps in order. Just for a quick implementation, let’s train a network that learns to predict
# `0.5` for every input of 10 floats. `Flux` defines the `train!` function to do it for us.

data, labels = rand(10, 100), fill(0.5, 2, 100)

# ╔═╡ e2e5701c-a635-4fb9-afd5-a2689632abc0
rand_test = cat(rand_test..., dims = 4) |> gpu

# ╔═╡ 2f277a73-9f75-4071-a836-0a2f1f422891
# The outputs are energies for the 10 classes. Higher the energy for a class, the more the
# network thinks that the image is of the particular class. Every column corresponds to the
# output of one image, with the 10 floats in the column being the energies.

# Let's see how the model fared.

rand_test = getarray.(image.(valset[ids]))

# ╔═╡ 3d8c01d0-8763-43aa-b82d-b44e2651d841
# While this is a valid way of updating our weights, it can get more complicated as the
# algorithms we use get more involved.

# Flux comes with a bunch of pre-defined optimisers and makes writing our own really simple.
# We just give it the learning rate η

opt = Descent(0.01)

# ╔═╡ 5ae30c0b-e6c5-44a9-ad54-754a50e4970a
using Plots

# ╔═╡ 5b082cfa-5a8a-4125-a809-673c1ed56460
opt = Momentum(0.01)

# ╔═╡ ef13440c-eac6-4e34-bff1-b20a96b5d54f
loss(x, y) = Flux.Losses.crossentropy(m(x), y)

# ╔═╡ 352a6d52-7628-46be-a013-c1b10891a3ac
begin
	import Pkg
	Pkg.add("Flux")
	Pkg.add("Plots")
	using Plots
end

# ╔═╡ 5ef7da96-8472-4e88-a921-7d48b7c5b3cc
# In simple cases it's pretty easy to work out the gradient by hand – here it's
# `6x+2`. But it's much easier to make Flux do the work for us!

using Flux

# ╔═╡ 319ea1b6-8be4-4b79-b8c2-e5054b9b516a
labels = onehotbatch([valset[i].ground_truth.class for i in 1:10000],1:10)

# ╔═╡ 93c61283-4a4c-45b3-ad21-1d9029f8847a
# Seeing our training routine unfold gives us an idea of how the network learnt the
# This is not bad for a small hand-written network, trained for a limited time.

# Training on a GPU
# -----------------

# The `gpu` functions you see sprinkled through this bit of the code tell Flux to move
# these entities to an available GPU, and subsequently train on it. No extra faffing
# about required! The same bit of code would work on any hardware with some small
# annotations like you saw here.

# ## Testing the Network
# ----------------------

# We have trained the network for 10 passes over the training dataset. But we need to
# check if the network has learnt anything at all.

# We will check this by predicting the class label that the neural network outputs, and
# checking it against the ground-truth. If the prediction is correct, we add the sample
# to the list of correct predictions. This will be done on a yet unseen section of data.

# Okay, first step. Let us perform the exact same preprocessing on this set, as we did
# on our training set.

valset = valimgs(CIFAR10)

# ╔═╡ 6dd286d8-89da-4960-9945-a3d02138e8d4
labels = onehotbatch([X[i].ground_truth.class for i in 1:50000],1:10)

# ╔═╡ 8ec62de5-4453-409e-81b4-403cc248c4c4
valset = 49001:50000

# ╔═╡ 2767abbd-29ad-4268-962b-5e15ee947367
loss(x, y) = Flux.Losses.crossentropy(m(x), y)

# ╔═╡ fbc8cdd5-87f4-4712-aabd-77cce4876b13
using Flux, Flux.Optimise

# ╔═╡ Cell order:
# ╠═352a6d52-7628-46be-a013-c1b10891a3ac
# ╠═6658db9f-6178-4f9b-b30e-633769648545
# ╠═1ed3883f-91fa-4653-aa79-af916c454f32
# ╠═5ef7da96-8472-4e88-a921-7d48b7c5b3cc
# ╠═123d36ad-ccf2-4395-8924-1da5b7faa93e
# ╠═86c7af2d-8170-4db8-9ac8-31c23da3d738
# ╠═494cb53e-f54b-4a9a-ab89-b1181f2d9058
# ╠═ececa7d9-5f25-480d-84c1-b5768b235f84
# ╠═5ae30c0b-e6c5-44a9-ad54-754a50e4970a
# ╠═68905575-c140-4b4a-9990-37c6e5b578d9
# ╠═ae800d10-4a23-4146-89e6-552650f3ef53
# ╠═b1eb6ba5-9d0b-4107-8780-05e8a7a329f6
# ╠═dd268469-21c0-4d73-9ad9-6bd40a1003c5
# ╠═cbad2052-d1c7-43b0-9734-2066c395f8ce
# ╠═5fd1cb23-cfd5-468a-9523-b907ebc6809d
# ╠═5827b3aa-4548-49ce-904a-82b8a30597d1
# ╠═21456496-e04f-4a60-b912-dc1b85abce29
# ╠═328e3bbd-dc7d-468d-b758-479a5f5e6fe9
# ╠═7c1017ae-4542-4b0b-9f9c-ceb5cf989484
# ╠═ddee81ba-e286-4a6e-a149-6284c5cb79f3
# ╠═a1e44848-e309-4ee3-b562-e3e1076efcc9
# ╠═6b993e29-8428-4bc8-94d5-0c1df5091170
# ╠═ac000fe2-6b93-47f3-9daf-6cd803ede884
# ╠═9ceed1a3-f041-4b01-8d4c-74af7c76b6ab
# ╠═bdab5d62-cdb7-4a9d-932a-f0915613a550
# ╠═c759656e-b6ad-4a3f-b784-3801e2946e00
# ╠═3d8c01d0-8763-43aa-b82d-b44e2651d841
# ╠═3869a8db-c15c-4310-bff0-198300485feb
# ╠═ef13440c-eac6-4e34-bff1-b20a96b5d54f
# ╠═49dfe241-1a71-47d8-b5f4-fd128afe90e7
# ╠═eeb695fb-7f2d-4226-9657-62db80025003
# ╠═fbc8cdd5-87f4-4712-aabd-77cce4876b13
# ╠═970f2eda-a2a9-4e05-b8cf-3f668fd0182f
# ╠═bfb80a92-9a13-4ee9-8577-53b1d11824bc
# ╠═b74db7f3-03fd-42fe-8e89-e22c19e10a5d
# ╠═ebb70637-1ecb-4dff-9a29-94f5e69f46dd
# ╠═fc1a4d57-7d6d-46d2-867c-3c1df6f580b7
# ╠═50dbd980-b3f8-4289-a926-ec58fa48f640
# ╠═2b88e19c-ac46-4d05-b2ab-6121339dd957
# ╠═75963a80-e623-4d45-9967-0c4580e310e5
# ╠═6dd286d8-89da-4960-9945-a3d02138e8d4
# ╠═7fde1f5d-f662-478d-b7ee-a5d05b2f6f4d
# ╠═ce116155-5152-4a05-a714-1c96f022cb2b
# ╠═b7b28239-e24e-4514-bc7b-1f9091ce1fb1
# ╠═a5e0fb39-34dc-47ca-a3a4-4ea3f9652545
# ╠═250c9353-19e8-452a-ae90-bf6d251eb6d8
# ╠═ea9a29cb-fa64-4573-973d-3acd47386cda
# ╠═8ec62de5-4453-409e-81b4-403cc248c4c4
# ╠═3153c68e-977f-46d6-904b-f81e1260b079
# ╠═ae42ed84-6c60-48fc-9e9b-7a58ef934b98
# ╠═3c9b18f7-870b-4e0a-a5b2-e0da83351127
# ╠═d635c2a1-1324-4219-b671-6b519c7735b6
# ╠═2767abbd-29ad-4268-962b-5e15ee947367
# ╠═5b082cfa-5a8a-4125-a809-673c1ed56460
# ╠═478ac041-a91a-4fda-a046-0e9498cd3afb
# ╠═7d180195-0508-4d81-9588-1c73b68fafe7
# ╠═a370264f-b115-4c6a-8740-2a5a4197cd38
# ╠═93c61283-4a4c-45b3-ad21-1d9029f8847a
# ╠═1ae9fe36-3b80-4224-8220-ab4f0e9c73a5
# ╠═319ea1b6-8be4-4b79-b8c2-e5054b9b516a
# ╠═f0c5c78c-da29-461b-af41-81f3b3f105bd
# ╠═cc63f580-c61d-4e82-9045-547dbca1bf90
# ╠═ca067855-e06b-4669-8920-5a141bc017ad
# ╠═2f277a73-9f75-4071-a836-0a2f1f422891
# ╠═e2e5701c-a635-4fb9-afd5-a2689632abc0
# ╠═b7bd6f58-fb6a-49f6-9caf-a9eaa9dc8ce0
# ╠═39158790-94ae-4f0b-9591-5bcc647fa373
# ╠═1d49bcdc-b29a-454d-a903-eaa12be03da7
# ╠═7327657f-a6c0-4151-87d0-74380324f18e
# ╠═2dbb3852-77b0-45be-a824-81b48e324a68
# ╠═97843034-3187-4a87-a53f-1800956e742f
# ╠═366a45f6-d80f-4ec3-ad5e-5226e03f00bd
# ╠═ea741139-6b55-4c59-8b4a-e0e4705a9180
