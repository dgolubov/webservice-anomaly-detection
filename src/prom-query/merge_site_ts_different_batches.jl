import FileIO
import Chain: @chain

"""
    mergeSiteMetricBatches(dirname,sitename)

Each batch of site metrics includes only 2 weeks worth of data, so ideally we would want to merge datasets from many weeks
Given a set of measurements for a single site in dir `site` like:
    site: {site1.jld2, site2.jld2, site3.jld2}
We can merge them into a single site.jld2
"""
function mergeSiteMetricBatches(dirname,sitename)
  @info "Loading site metrics from files in $dirname"
  batches= @chain dirname readdir(join=true) FileIO.load.(_)
  merged= Dict("metadata"=> [batches[i]["metadata"] for i in 1:length(batches)],
               "ts"=> vcat((batches[i]["ts"] for i in 1:length(batches)...)))
  FileIO.save("$dirname/../$sitename.jld", merged)
  @info "Saved merged data to \"$dirname/../$sitename.jld\""
  merged
end
