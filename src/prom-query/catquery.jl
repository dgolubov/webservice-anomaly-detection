# Random musings

"`catQuery(a, b)` concatenates the strings with `&`"
catQuery(a,b)= catQuery(a|>keyvalStr, b|>keyvalStr)
catQuery(a...)= catQuery(Base.front(a)...)|> catQuery(last(a))
catQuery(b)= a-> catQuery(a,b)
catQuery(a::AbstractString,b::AbstractString)= startswith(b,"&") ? a * b : a * "&" * b
"Convenience method for dicts"
catQuery(d::AbstractDict)= accumulate(catQuery,d)[end]
keyvalStr(a)= string(a[1])*"="*string(a[2])
keyvalStr(a::AbstractString)= a
keyvalStr(a::Dict)= catQuery(a)