readClusterToken(clusterName)= YAML.load_file(params["getPrometheusMetrics"]["secretsFile"])["clusterToken"][clusterName]

"""
    queryTime(timespan, timestep)
Standardized time range for a query. Start: `now - timespan`, end: `now`
"""
queryTime(timespan, timestep)= Dict(
  "start"=> ((now() - timespan)|> string) * "Z",
  "end"=> (now()|> string) * "Z",
  "step"=> timestep,
  )

"Number of points in the prometheus query"
queryPoints(qtime)= begin
  tend= qtime["end"][1:end-1]|> DateTime
  tstart= qtime["start"][1:end-1]|> DateTime
  step= qtime["step"][1:end-1]|> Dates.Second
  @chain Dates.Second(tend-tstart)/step ceil(Int, _)
end

"""
    timedQueries(query, time)
Add time info to query, splitting it into multiple queries if the number of points exceeds the max allowed by Prometheus
"""
function timedQueries(query, time)
  maxPoints= params["getPrometheusMetrics"]["prometheusMaxPointsPerQuery"]
  map(queryTime.(splitrange(time, maxPoints))) do time
    merge(Dict("query"=> query["query"]), time)
  end
end

"Get query_range from the cluster's prometheus HTTP API"
function getPrometheusTimeseries(query; clusterName)
  httpConfig= NamedTuple{Tuple(Symbol.(keys(params["getPrometheusMetrics"]["httpConfig"])))}(values(params["getPrometheusMetrics"]["httpConfig"]))
  getPromData= query -> @chain query begin
    HTTP.URI(scheme= "https",
      host= "prometheus-k8s-openshift-monitoring." * clusterName * ".cern.ch",
      path= "/api/v1/query_range",
      query= _)
    HTTP.get(headers=["Authorization" => "Bearer " * readClusterToken(clusterName)]; httpConfig...)
  end
  @chain getPromData begin
    retryServerErrors(_)(query)
    HTTP.body
    promqueryRespParse
  end
end
promqueryRespParse(respBody)=
  @chain respBody begin
    String
    JSON.parse(_)["data"]["result"]
  end

"""
`dataframeFromPrometheusTimeseries(ts; metricDataType=Float64)` creates a DataFrame from Prometheus JSON responses.
`ts:= promethHTTPresp["data"]["result"][1]["values"]
"""
dataframeFromPrometheusTimeseries(ts; metricDataType=Float64)= DataFrame(
  time= map(x-> Dates.unix2datetime(x[1]), ts),
  value= map(x-> parse(metricDataType,x[2]), ts))

"""
Given a prometheus timeseries with its metrics and query (`promethHTTPresp["data"]["result"][1]`)`,
repackage it in dataframe + metadata dict format
"""
timeseriesAndMeta(prometheusTs, query)= dataframeFromPrometheusTimeseries(prometheusTs["values"]), merge(prometheusTs["metric"], query)
timeseriesAndMeta(::Missing, query)= missing, missing

"Iterator for site names read from file"
sitenameGen(clusterName)= eachline(joinpath(params["sitenamesDir"], clusterName))

getPrometheusDataForQuery(query,time,clusterName)= begin
  prometheusDataSingleQuery(query)= @chain query begin
      getPrometheusTimeseries(_, clusterName=clusterName)
      (x -> length(x) < 1 ? missing : x[1])(_)    # replace no data with missing
    end
  # Return time series for a potentially long query that might be split into an array of multiple queries
  @chain begin
    prometheusDataSingleQuery.(timedQueries(query,time))
    mergeTimeseries
    timeseriesAndMeta(_, merge(query,time))
  end
end

"merge an array of (timeseries, metadata) pairs"
function mergeTimeseries(data)
  takeVal(::Missing)= missing
  takeVal(x)= x["values"]
  Dict(
    "metric"=> data[1]["metric"],
    "values"=> map(takeVal, data)|> skipmissing|> Iterators.flatten|> collect
    )
end
mergeTimeseries(::Vector{Missing})= missing

"df, metadata= `mergeDataframes(dataChan)` takes all items from `dataChan` and merges them into a single dataframe"
function mergeDataframes(dataChan)
  _mergedf(::Missing, _)= missing, missing
  _mergedf(mergedDf,metadata)= begin
    rename!(mergedDf, "value"=>metadata["name"])
    for (df, meta) in dataChan
      rename!(df, "value"=>meta["name"])
      mergedDf= innerjoin(mergedDf,df, on=:time)
    end
    mergedDf,metadata
  end
  @chain dataChan take! _mergedf(_...)
end

function savePrometheusDataForCluster(clusterName)
  # Use the same time for all requested data
  qtime= queryTime(timespan,timestep)
  failedSites= Channel{String}(3000)
  @sync foreach(sitenameGen(clusterName)) do sitename
    @async begin
      try
        tsChan= Channel(queries(sitename)|> length)
        # Launch all query HTTP requests concurrently
        # and synchronize at the end to simplify error handling (otherwise we need an additional mechanism to inform the consumer)
        # without `@sync`, error handling won't work!
        # see https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/#go-statement-considered-harmful
        @sync foreach(queries(sitename)) do query
          @async @chain query begin
            getPrometheusDataForQuery(_, qtime, clusterName)
            put!(tsChan, _)
          end
        end
        close(tsChan)
        @info "$sitename: fetched metrics"
        #@chain tsChan mergeDataframes saveF(_..., clusterName=clusterName, sitename=sitename)
        ts, metadata= mergeDataframes(tsChan)
        save(joinpath(datasetDir(clusterName), sitename * ".jld2"), "metadata", metadata, "ts", ts)
      catch e
        @warn "$sitename: failed to fetch metrics" e
        put!(failedSites,sitename)
      end
    end
  end
  @info "-> Fetched all websites for cluster $clusterName"
  failedSitesList= []
  while isready(failedSites) push!(failedSitesList, take!(failedSites)) end
  length(failedSitesList) > 0 && ( @warn "Failed sites:" failedSitesList )
end
