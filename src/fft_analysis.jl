using FFTW, JLD2, DataFrames, Plots, DSP
@load "data/prometheusmetrics/webeos/lhcbproject.jld2"

fft= FFTW.rfft(ts[!,"response latency"])
plot(fft.|>abs)

##
using Random, Distributions, StatsBase, StateSpaceModels
import StateSpaceModels: fit!, results
d = Distributions.Normal(0,1)
x= rand(d,10000)

model = [SARIMA(x; order=(5,0,0)),
  SARIMA(x|>cumsum; order=(5,0,0))]
model.|> fit!
model.|> results
