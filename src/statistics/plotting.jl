using Plots
using FileIO
using DataFrames
using TimeSeries
using StatsBase
using Statistics
using Chain
using Printf
using DSP
using TSne
using LinearAlgebra

function load_data(filename) 
    df = @chain filename joinpath("data/prometheusmetrics/webeos", _) load(_)["ts"]
    df[isnan.(df."error rate (15m)"), :"error rate (15m)"] .= 0 #set NaN error rates to zero
    err = df[!,"error rate (15m)"]
    m = maximum(err[err .!= Inf])
    df[isinf.(df."error rate (15m)"), :"error rate (15m)"] .= 10*m #set Inf error rates to 10max
    df
end
lhcdf = load_data("lhcbproject.jld2")
describe(lhcdf)|>display
#lhcdf.group = repeat(1:5, inner=15)

ts = TimeArray(lhcdf, timestamp = :time)
mat = values(ts)

function featureNormalize(X)
    mu = mean(X,dims=1)
    sigma = std(X,dims=1)
    
    X_norm = (X .- mu) ./ sigma
    
    return X_norm
end
mat2 = featureNormalize(mat)
mts = [mat2[i-14:i,:] for i in collect(16:15:9923)]


function EROS(i,j,v,w) 
    eros = 0.0
    for l in collect(1:7)
        eros += w[l]*dot(v[i,:,l],v[j,:,l])
    end
    eros
end

function computeWeightRaw(S)
    n = size(S)[2]
    w = Vector{Float64}(undef,n)
    for i in collect(1:n)
        w[i] = mean(S[:,i])
    end
    replace!(w, NaN => 0)
    w = w./sum(w)
end




S = zeros(Float64, (length(mts),7));
v = zeros(Float64, (length(mts),7,7));
for k in eachindex(mts)
    A = mts[k]
    B = cov(A)
    F = svd(B)
    S[k,:] .= F.S
    v[k,:,:] .= F.V
end
w = computeWeightRaw(S)
N = length(mts)
dist_mat = zeros(Float64,(N,N));
for i in collect(1:N)
    for j in collect(1:N)
        dist_mat[i,j] = EROS(i,j,v,w)
    end
end

t = tsne(dist_mat,distance = true)
scatter(t[:,1],t[:,2],color = Int.(round.([sum(mts[i]) for i in collect(1:size(dist_mat)[1])])))


function spect(filename)
    df = load_data(filename)
    connect = df[!,"connection irate"]
    mean_centered = connect .- mean(connect)
    p = spectrogram(mean_centered, fs = 1/60, window = hamming)
    heatmap(power(p),ylabel = "Frequency", xlabel = "Time bin")
    savefig(@sprintf "plots/%s:spectrogram" replace(filename, ".jld2" => ""))
end

function plotting(filename)
    df = load_data(filename)
    columns = (names(df))[(begin+1):end]
    for column in columns
        p = plot(df[!,"time"], df[!,column], xlabel = "Time", title = "Time Series", ylabel = @sprintf "%s" column)
        try 
            acf = plot(autocor(df[!,column]), xlabel = "Lag", ylabel = "Correlation", title = "ACF", ylims = (0,1))
            pac = plot(pacf(df[!,column],collect(1:37)), xlabel = "Lag", ylabel = "Correlation", title = "PACF", ylims = (0,1))
            plot(p, acf, pac, layout = 3)|>display
        catch e
            plot(p)|>display
        end
        savefig(@sprintf "plots/%s:%s" replace(filename, ".jld2" => "") replace(column, " " => "-"))
    end
end

function crosscovariance(filename)
    df = load_data(filename)
    ts = TimeArray(df, timestamp = :time)
    mat = values(ts)
    c = crosscor(mat,mat, 0:40)
    columns = (names(df))[(begin+1):end]
    for i in eachindex(columns)
        for j in 1:(i-1) # We go through the indices less than current column, so we dont go through it twice
            if any(c[:,i,j] .> 0.15) #If correlation exceeds threshold then plot it
                plot(c[:,i,j],ylims = (0,1),xlab = "Lags", ylab = "Correlation",title = @sprintf "Cross corr.: %s and %s" columns[i] columns[j])|>display
                savefig(@sprintf "plots/%s:correlation:%s:%s" replace(filename, ".jld2" => "") replace(columns[i], " " => "-") replace(columns[j], " " => "-"))
            end
        end
    end
end