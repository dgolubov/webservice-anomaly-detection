using TSne, Statistics, MLDatasets, Random
Random.seed!(1234)
rescale(A; dims=1) = (A .- mean(A, dims=dims)) ./ max.(std(A, dims=dims), eps())

alldata, allabels = MNIST.traindata(Float64);
data = reshape(permutedims(alldata[:, :, 1:2500], (3, 1, 2)),
               2500, size(alldata, 1)*size(alldata, 2));
# Normalize the data, this should be done if there are large scale differences in the dataset
X = rescale(data, dims=1);

Y_2D = tsne(X, 2, 50, 1000, 20.0);
Y_3D = tsne(X, 3, 50, 1000, 20.0);

using Plots
plot2D = scatter(Y_2D[:,1], Y_2D[:,2], marker=(2,2,:auto,stroke(0)), color=Int.(allabels[1:size(Y_2D,1)]))
plot3D = scatter(Y_3D[:,1], Y_3D[:,2], Y_3D[:,3], marker=(2,2,:auto,stroke(0)), color=Int.(allabels[1:size(Y_3D,1)]))